package ru.manufacture.jaction.util;

import java.util.Calendar;

/**
 * @author Degtyarev Roman
 * @date 13.10.2015.
 */
public class DummyTest {
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        calendar.set(Calendar.MONTH, 0);
        System.out.println(calendar.getTime());
        System.out.println(month);
    }
}
