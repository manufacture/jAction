package ru.manufacture.jaction.util;

import ru.manufacture.spring.Spring;

/**
 * @author Degtyarev Roman
 * @date 16.09.2015.
 */
public class Hibernate extends ru.manufacture.spring.Hibernate {
    private static final Hibernate hibernate = new Hibernate();

    private Hibernate() {
        super.sessionFactory = Spring.bean("sessionFactory");
    }

    public static Hibernate instance() {
        return hibernate;
    }

    public static void flush() {
        hibernate.session().flush();
    }
}
