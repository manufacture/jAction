package ru.manufacture.jaction.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.jdbc.support.JdbcUtils;

/**
 * @author Degtyarev Roman
 * @date 12.10.2015.
 */
public class JdbcTest extends Assert {

    public static final String DRIVER = "oracle.jdbc.driver.OracleDriver";
    public static final String URL = "jdbc:oracle:thin:@KC-11-101.bss.lan:1521:CORREQTS";
    public static final String USER = "ILIK_STAND11";
    public static final String PASSWORD = "password";

    @Test
    public void testConnect() throws ClassNotFoundException, SQLException {
        Class.forName(DRIVER);
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            statement = connection.prepareStatement("select count(*) from DOCCONTENT_CONSULTREQUEST");
            rs = statement.executeQuery();
            if (rs.next()) {
                System.out.println("count(*) = " + rs.getLong(1));
            } else {
                throw new IllegalStateException("oops");
            }
        } catch (Exception ignore) {}
        finally {
            JdbcUtils.closeResultSet(rs);
            JdbcUtils.closeStatement(statement);
            JdbcUtils.closeConnection(connection);
        }
    }
}
