package ru.manufacture.jaction.lang;

import java.io.IOException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.manufacture.jaction.support.ProcessEngineTest;

/**
 * @date 02.08.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:META-INF/spring/applicationContext*.xml"})
public class ImportTest extends ProcessEngineTest {

    @Test
    public void execute() throws IOException {
        deploy("process/roles.xml");
        deploy("process/algorithms.xml");
        deploy("process/milestones.xml");
        String process = deploy("process/process[demo1].xml");
        System.out.println(process);
    }
}
