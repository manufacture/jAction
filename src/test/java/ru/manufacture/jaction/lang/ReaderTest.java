package ru.manufacture.jaction.lang;

import java.nio.file.Path;
import java.nio.file.Paths;
import org.dom4j.DocumentException;
import org.junit.Assert;
import org.junit.Test;
import ru.manufacture.jaction.model.Declaration;
import ru.manufacture.jaction.model.ProcessDescriptor;
import ru.manufacture.jaction.util.Reader;
import ru.manufacture.util.io.Xml;

/**
 * @author Degtyarev Roman
 * @date 23.06.2015.
 */
public class ReaderTest extends Assert {
    @Test
    public void readTest() throws DocumentException {
        String userDir = System.getProperty("user.dir");
        Path xml = Paths.get(userDir, "src", "test", "resources", "process", "process[1].xml");
        Declaration declaration = new Reader().read(Xml.read(xml));
        System.out.println(declaration);
    }
}
