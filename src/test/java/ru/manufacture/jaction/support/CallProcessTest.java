package ru.manufacture.jaction.support;

import org.junit.Before;
import org.junit.Test;
import ru.manufacture.jaction.protocol.CompleteAction;
import ru.manufacture.jaction.protocol.ProcessRequest;
import ru.manufacture.jaction.protocol.ProcessResponse;
import ru.manufacture.jaction.protocol.StartProcess;

/**
 * @author Degtyarev Roman
 * @date 09.03.2016.
 */
public class CallProcessTest extends ProcessEngineTest {
    @Before
    public void before() throws Exception {
        super.before();
        deploy("src/test/resources/process/process[1].xml");
        deploy("src/test/resources/process/process[2].xml");
    }

    @Test
    public void execute() {
        StartProcess<String> startProcess = new StartProcess<>(user, "process[1]", new ProcessRequest<>("hallo"));
        ProcessResponse<String> response = startProcess.invoke();
        System.out.println("Current action: " + response.getActionId());
        System.out.println("UI form: " + response.getView());
        System.out.println("Form data: " + response.getData());
        CompleteAction<String> complete = new CompleteAction<>(user, response.getActionId(), new ProcessRequest<>("world"));
        response = complete.invoke();
        System.out.println(response);
    }
}
