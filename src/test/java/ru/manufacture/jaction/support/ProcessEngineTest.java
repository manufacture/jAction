package ru.manufacture.jaction.support;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import ru.manufacture.jaction.model.ProcessDescriptor;
import ru.manufacture.jaction.model.view.ActionInstanceFilter;
import ru.manufacture.jaction.model.view.VActionInstance;
import ru.manufacture.jaction.protocol.CompleteAction;
import ru.manufacture.jaction.protocol.DeployProcess;
import ru.manufacture.jaction.protocol.ExecuteAction;
import ru.manufacture.jaction.protocol.GetActionInstances;
import ru.manufacture.jaction.protocol.ProcessRequest;
import ru.manufacture.jaction.protocol.ProcessResponse;
import ru.manufacture.jaction.protocol.StartProcess;
import ru.manufacture.jaction.security.IGroup;
import ru.manufacture.jaction.security.IRole;
import ru.manufacture.jaction.security.IUserCredential;
import ru.manufacture.jaction.util.Hibernate;
import ru.manufacture.jaction.util.Source;
import ru.manufacture.java.ClassPath;
import ru.manufacture.spring.Spring;
import ru.manufacture.spring.Transaction;
import ru.manufacture.types.Interval;
import ru.manufacture.types.format.Format;
import ru.manufacture.util.io.Xml;
import ru.manufacture.util.pageable.SimplePageable;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:META-INF/spring/applicationContext*.xml"})
public class ProcessEngineTest extends Assert {
    protected IUserCredential user;
    private Transaction transaction;

    @Before
    public void before() throws Exception {
        user = createUser("obama", roles("actor1", "actor2"), Arrays.asList(createGroup("branch1")));
        transaction = new Transaction() {
            @Override
            protected PlatformTransactionManager getTransactionManager() {
                return Spring.bean("transactionManager");
            }
        };
        transaction.begin();
    }

    @After
    public void after() {
        transaction.commit();
    }

    private IUserCredential createUser(final String userName, final Collection<? extends IRole> roles, final List<IGroup> groups) {
        return new IUserCredential() {
            @Override
            public String getLogin() {
                return userName;
            }

            @Override
            public Set<IRole> getRoles() {
                return new HashSet<>(roles);
            }

            @Override
            public Set<IGroup> getGroups() {
                return new HashSet<>(groups);
            }
        };
    }

    private IGroup createGroup(final String groupName) {
        return new IGroup() {
            @Override
            public String getName() {
                return groupName;
            }
        };
    }

    private Collection<? extends IRole> roles(String... roles) {
        List<IRole> result = new LinkedList<IRole>();
        for (final String role : roles) {
            result.add(new IRole() {
                @Override
                public String getName() {
                    return role;
                }

                @Override
                public Type getType() {
                    return Type.HUMAN;
                }
            });
        }
        return result;
    }

    @Test
    public void execute() throws IOException {
        String processName = deploy("src/test/resources/process/process[2].xml");
        StartProcess<String> startProcess = new StartProcess<>(user, processName, new ProcessRequest<>("hallo"));
        ProcessResponse<String> response = startProcess.invoke();
        System.out.println(response);

        ProcessRequest<String> request = new ProcessRequest<>("hallo");
        ExecuteAction<String> executeAction = new ExecuteAction<>(user, response.getActionId(), request);
        response = executeAction.invoke();
        System.out.println(response);

        Hibernate.flush();

        Date date = Format.toObject(Date.class, "15.09.2015");

        ActionInstanceFilter filter = new ActionInstanceFilter(Interval.from(date));
        GetActionInstances getActionInstances = new GetActionInstances(user, filter);
        SimplePageable<VActionInstance> page = getActionInstances.invoke();
        long totalRows = page.getTotalRows();
        List<VActionInstance> actions = page.getData();
        System.out.println("total open actions: " + totalRows);
        System.out.println("current page actions: " + actions);

        CompleteAction<String> completeAction = new CompleteAction<>(user, response.getActionId(), new ProcessRequest<>("world"));
        response = completeAction.invoke();
        System.out.println(response);

        completeAction = new CompleteAction<>(user, response.getActionId(), new ProcessRequest<>("world"));
        completeAction.invoke();
    }

    public String deploy(String path) throws IOException {
        String projectHome = new File("").getAbsolutePath();
        System.setProperty(ClassPath.getBaseDirKey(), projectHome + File.separator + "target");
        System.setProperty(ClassPath.getLocationsKey(), "classes;test-classes");

        Source descriptorSource;

        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource resource = resolver.getResource(path);
        if (resource.exists()) {
            descriptorSource = new Source(resource.getInputStream(), "UTF-8");
        } else {
            Path descriptorPath = Paths.get(path);
            if (Files.exists(descriptorPath)) {
                descriptorSource = new Source(descriptorPath, "UTF-8");
            } else {
                throw new FileNotFoundException("Resource not found: " + path);
            }
        }

        DeployProcess deploy = new DeployProcess(user, descriptorSource);
        String[] strings = deploy.invoke();
        return strings.length > 0 ? strings[0] : "";
    }
}