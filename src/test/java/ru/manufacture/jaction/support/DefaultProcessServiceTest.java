package ru.manufacture.jaction.support;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.junit.Test;
import ru.manufacture.jaction.model.Group;
import ru.manufacture.jaction.protocol.DeployProcess;
import ru.manufacture.jaction.security.IGroup;
import ru.manufacture.jaction.security.IRole;
import ru.manufacture.jaction.security.IUserCredential;

public class DefaultProcessServiceTest extends ProcessEngineTest {

    @Test
    public void testDeployProcess() throws Exception {
        DeployProcess deploy = new DeployProcess(getUserCredential(), "classpath*:process/*.xml");
        String[] processNames = deploy.invoke();
        System.out.println("process deployed: " + Arrays.asList(processNames));
    }

    public IUserCredential getUserCredential() {
        return new IUserCredential() {
            @Override
            public String getLogin() {
                return "root";
            }

            @Override
            public Set<IRole> getRoles() {
                return new HashSet<IRole>(){{add(new IRole() {
                    @Override
                    public String getName() {
                        return "Administrator";
                    }

                    @Override
                    public Type getType() {
                        return Type.HUMAN;
                    }
                });}};
            }

            @Override
            public Set<IGroup> getGroups() {
                return new HashSet<IGroup>(){{add(Group.Any);};};
            }
        };
    }
}