package ru.manufacture.demo;

import ru.manufacture.jaction.model.ActionInstance;
import ru.manufacture.jaction.protocol.ProcessRequest;
import ru.manufacture.jaction.support.algorithm.DefaultAlgorithmEngine;

/**
 * @author Degtyarev Roman
 * @date 22.04.2016.
 */
public class DemoAlgorithmEngine extends DefaultAlgorithmEngine {
    @Override
    public <Data> Data executeAction(ActionInstance actionInstance, ProcessRequest<Data> request) {
        System.out.println("execute: " + actionInstance.getAction().getAlgorithm().getSource());
        return super.executeAction(actionInstance, request);
    }
}
