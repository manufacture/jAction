package ru.manufacture.demo;

import ru.manufacture.jaction.model.Action;
import ru.manufacture.jaction.model.ActionInstance;
import ru.manufacture.jaction.security.IGroup;
import ru.manufacture.jaction.support.dispatch.DefaultProcessDispatcher;

/**
 * @author Degtyarev Roman
 * @date 22.04.2016.
 */
public class DemoProcessDispatcher extends DefaultProcessDispatcher {
    @Override
    public IGroup dispatch(ActionInstance actionInstance) {
        Action action = actionInstance.getAction();
        System.out.println("dispatch action: " + action.getName() + "[" + action.getRole() + "]");
        return super.dispatch(actionInstance);
    }
}
