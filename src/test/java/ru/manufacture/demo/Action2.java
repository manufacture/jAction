package ru.manufacture.demo;

import ru.manufacture.jaction.protocol.ProcessRequest;
import ru.manufacture.jaction.support.algorithm.CompleteAction;
import ru.manufacture.jaction.support.algorithm.CreateDescription;
import ru.manufacture.jaction.support.algorithm.JAction;
import ru.manufacture.jaction.support.algorithm.ExecuteAction;

/**
 * @author Degtyarev Roman
 * @date 20.10.2015.
 */
public class Action2<Data> extends JAction implements ExecuteAction<Data>, CompleteAction<Data>, CreateDescription {
    @Override
    public void completeAction(ProcessRequest<Data> request) {

    }

    @Override
    public String createDescription() {
        return "Action2";
    }

    @Override
    public Data executeAction(ProcessRequest<Data> request) {
        return request.getData();
    }
}
