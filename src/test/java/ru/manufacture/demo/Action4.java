package ru.manufacture.demo;

import ru.manufacture.jaction.protocol.ProcessRequest;
import ru.manufacture.jaction.support.algorithm.CreateDescription;
import ru.manufacture.jaction.support.algorithm.JAction;
import ru.manufacture.jaction.support.algorithm.ExecuteSystemAction;

/**
 * @author Degtyarev Roman
 * @date 20.10.2015.
 */
public class Action4<Data> extends JAction implements ExecuteSystemAction<Data>, CreateDescription {
    @Override
    public void executeSystemAction(ProcessRequest<Data> request) {
        Data data = request.getData();
    }

    @Override
    public String createDescription() {
        String next = getVar("NEXT");
        return getActionInstance().getAction().getName() + " " + next;
    }
}
