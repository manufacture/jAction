package ru.manufacture.demo;

import ru.manufacture.jaction.model.ProcessInstance;
import ru.manufacture.jaction.support.hook.DefaultProcessHook;

/**
 * @author Degtyarev Roman
 * @date 22.04.2016.
 */
public class DemoProcessHook extends DefaultProcessHook {
    @Override
    public void onStart(ProcessInstance instance) {
        System.out.println("start process: " + instance.getProcess());
    }
}
