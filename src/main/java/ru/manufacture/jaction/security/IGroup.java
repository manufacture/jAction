package ru.manufacture.jaction.security;

import java.io.Serializable;
import ru.manufacture.persist.Named;

/**
 * @author Degtyarev Roman
 * @date 31.07.2015.
 */
public interface IGroup extends Serializable, Named {
    String getName();
}
