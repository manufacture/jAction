package ru.manufacture.jaction.security;

import java.io.Serializable;
import java.util.Set;

/**
 * @author Degtyarev Roman
 * @date 31.07.2015.
 */
public interface IUserCredential extends Serializable {
    String getLogin();
    Set<IRole> getRoles();
    Set<IGroup> getGroups();
}
