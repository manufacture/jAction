package ru.manufacture.jaction.security;

import java.io.Serializable;
import ru.manufacture.persist.Named;

/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
public interface IRole extends Serializable, Named {
    public enum Type {
        SYSTEM, HUMAN;

        public static Type of(String value) {
            for (Type type : values()) {
                if (type.name().equalsIgnoreCase(value))
                    return type;
            }
            throw new IllegalArgumentException("Role type not supported: " + value);
        }
    }
    String getName();
    Type getType();
}
