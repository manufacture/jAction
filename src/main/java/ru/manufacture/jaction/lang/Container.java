/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
package ru.manufacture.jaction.lang;

import java.util.Arrays;
import java.util.List;

/**
 * контейнер операторов
 */
public abstract class Container extends Statement {
    /**
     * начальный индекс дочернего оператора
     */
    public static final int FIRST_INDEX = 0;
    /**
     * список дочерних операторов
     */
    private List<? extends Statement> statements;

    /**
     * конструктор
     * @param id идентификатор оператора в рамках схемы процесса
     * @param statements массив дочерних операторов
     */
    public Container(long id, Statement ... statements) {
        this(id, Arrays.asList(statements));
    }

    /**
     * конструктор
     * @param id идентификатор оператора в рамках схемы процесса
     * @param statements список дочерних операторов
     */
    public Container(long id, List<? extends Statement> statements) {
        super(id);
        this.statements = statements;
        int index = 0;
        for (Statement statement : statements) {
            statement.setIndex(index++);
            statement.setParent(this);
        }
    }

    /**
     * @return true - если список дочерних операторов пуст
     */
    public boolean isEmpty() {
        return statements.isEmpty();
    }

    /**
     * проверяет индекс (порядковый номер) оператора
     * @param index индекс (порядковый номер) оператора
     * @return true - если это начальный индекс
     * @see #FIRST_INDEX
     */
    public boolean isEnterIndex(int index) {
        return index <= FIRST_INDEX;
    }

    /**
     * проверяет индекс (порядковый номер) оператора
     * @param index индекс (порядковый номер) оператора
     * @return true - если этот идекс выходит за пределы числа операторов контейнера
     */
    public boolean isExitIndex(int index) {
        return index >= statements.size();
    }

    /**
     * Возвращает следующий оператор, который должен быть выполнен в соответствии со схемой процесса
     * @param statementIndex индекс (порядковый номер) текущего оператора в этом контейнере
     * @return следующий оператор для выполнения или null, если процесс завершен
     */
    public abstract Statement getNext(int statementIndex);

    /**
     * @return возвращает список операторов контейнера
     */
    public List<? extends Statement> getContent() {
        return statements;
    }

    /**
     * вложенный контейнер
     */
    public static class Nested extends Container {
        /**
         * тип оператора
         */
        private Type type;

        /**
         * конструктор
         * @param id идентификатор оператора в рамках схемы процесса
         * @param type тип оператора
         * @param statements список дочерних операторов
         */
        public Nested(long id, Type type, List<? extends Statement> statements) {
            super(id, statements);
            this.type = type;
        }

        /**
         * @see super#getNext(int)
         */
        @Override
        public Statement getNext(int statementIndex) {
            if (isEmpty() || isExitIndex(statementIndex)) {
                Container parent = getParent();
                return parent.getParent().getNext(parent.getIndex() + 1);
            }
            Statement statement = getContent().get(statementIndex);
            if (statement instanceof Container) {
                return ((Container)statement).getNext(FIRST_INDEX);
            } else {
                return statement;
            }
        }

        /**
         * @see super#getType()
         */
        @Override
        public Type getType() {
            return type;
        }
    }
}
