/**
 * @author Degtyarev Roman
 * @date 21.07.2015.
 */
package ru.manufacture.jaction.lang;

import ru.manufacture.jaction.model.Var;

/**
 * оператор установки значения переменной контекста процесса
 */
public class SetValue extends Statement {
    /**
     * переменная контекста процесса
     */
    private Var var;
    /**
     * строковое представление значения переменной
     */
    private Object value;

    /**
     * конструктор
     * @param id идентификатор оператора в рамках схемы процесса
     * @param var переменная контекста процесса
     * @param value строковое представление значения переменной
     */
    public SetValue(long id, Var var, String value) {
        this(id, var, var.getType().cast(value));
    }

    public SetValue(long id, Var var, Object value) {
        super(id);
        this.var = var;
        this.value = value;
    }

    /**
     * @return возвращает описание переменной контекста процесса
     */
    public Var getVar() {
        return var;
    }

    /**
     * @return возвращает строковое представление значения переменной
     */
    public String getStringValue() {
        return var.getType().toString(value);
    }

    /**
     * @return возвращает типизированное(приведенное к типу) значение переменной
     */
    public Object getValue() {
        return value;
    }

    /**
     * @see super#getType()
     */
    @Override
    public Type getType() {
        return Type.Set;
    }

    @Override
    public String toString() {
        return "set " + var.getName() + " := " + value;
    }
}
