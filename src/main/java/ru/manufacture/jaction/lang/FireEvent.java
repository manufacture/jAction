/**
 * @author Degtyarev Roman
 * @date 21.07.2015.
 */
package ru.manufacture.jaction.lang;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import ru.manufacture.jaction.model.Event;

/**
 * Оператор генерации события
 */
public class FireEvent extends Statement {
    /**
     * событие
     */
    private Event event;
    /**
     * локальные и глобальные параметры события
     */
    private Map<String, String> params;

    /**
     * конструктор
     * @param id идентификатор оператора в рамках схемы процесса
     * @param event событие
     * @param params локальные параметры события
     */
    public FireEvent(long id, Event event, Map<String, String> params) {
        super(id);
        this.event = event;
        this.params = new ConcurrentHashMap<>(event.getParams());
        this.params.putAll(params);
    }

    /**
     * @return возвращает событие
     */
    public Event getEvent() {
        return event;
    }

    /**
     * @return возвращает локальные и глобальные параметры события
     */
    public Map<String, String> getParams() {
        return params;
    }

    /**
     * @see super#getType()
     */
    @Override
    public Type getType() {
        return Type.Fire;
    }

    @Override
    public String toString() {
        return "fire event[" + event.getName() + "]: params" + params;
    }
}
