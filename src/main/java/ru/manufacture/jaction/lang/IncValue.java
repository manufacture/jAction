/**
 * @author Degtyarev Roman
 * @date 21.07.2015.
 */
package ru.manufacture.jaction.lang;

import java.math.BigDecimal;
import ru.manufacture.jaction.model.ProcessInstance;
import ru.manufacture.jaction.model.Var;
import ru.manufacture.jaction.support.ProcessService;


import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.isNumeric;

/**
 * оператор инкремента переменной контекста процесса
 */
public class IncValue extends SetValue {
    /**
     * конструктор
     * @param id идентификатор оператора в рамках схемы процесса
     * @param var переменная контекста процесса
     * @param value значение инкремента; если null или пусто, то используется значение по умолчанию 1
     */
    public IncValue(long id, Var var, String value) {
        super(id, var, isBlank(value) ? "1" : value);
        if (!isNumeric(getStringValue())) {
            throw new IllegalArgumentException("Increment numeric value required: " + getValue());
        }
    }

    /**
     * вычисляет значение инкремента переменной
     * @param processInstance экземпляр процесса, переменная которого инкрементируется
     * @return результат инкремента переменной
     */
    public Object getResultValue(ProcessInstance processInstance) {
        Var var = getVar();
        Object value = ProcessService.instance().loadVariable(processInstance, var);
        if (null == value) {
            if (!isBlank(var.getDefaultValue())) {
                value = var.getType().cast(var.getDefaultValue());
            } else {
                throw new NullPointerException("Variable is null: " + var);
            }
        }
        switch (var.getType()) {
            case Long: {
                return (Long) value + (Long) getValue();
            }
            case Integer: {
                return (Integer) value + (Integer) getValue();
            }
            case Byte: {
                return (Byte) value + (Byte) getValue();
            }
            case Short: {
                return (Short) value + (Short) getValue();
            }
            case Float: {
                return (Float) value + (Float) getValue();
            }
            case BigDecimal: {
                return ((BigDecimal) value).add((BigDecimal) getValue());
            }
            case Double: {
                return (Double) value + (Double) getValue();
            }
        }
        throw new IllegalArgumentException("Increment not supported for type: " + var.getType());
    }

    /**
     * @see super#getType()
     */
    @Override
    public Type getType() {
        return Type.Inc;
    }

    @Override
    public String toString() {
        return "set " + getVar().getName() + " := " + getVar().getName() + " + " + getStringValue();
    }
}
