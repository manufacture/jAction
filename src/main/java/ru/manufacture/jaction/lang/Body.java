/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
package ru.manufacture.jaction.lang;

import java.util.List;
import ru.manufacture.jaction.model.Milestone;

/**
 * Тело процесса - оператор предназначенный для объявления границ схемы процесса в тексте дескриптора;
 * Является контейнером содержащим операторы
 */
public class Body extends Container {
    private Milestone finalMilestone;
    /**
     * конструктор тела процесса
     * @param id идентификатор оператора уникальный в рамках этого процесса
     * @param content содержимое тела процесса (контейнера), т.е. операторы
     * @param finalMilestone финальный этап процесса
     */
    public Body(long id, List<? extends Statement> content, Milestone finalMilestone) {
        super(id, content);
        this.finalMilestone = finalMilestone;
    }

    /**
     * @see super#getNext(int)
     */
    @Override
    public Statement getNext(int statementIndex) {
        if (isEmpty() || isExitIndex(statementIndex)) {
            return null;
        }
        Statement statement = getContent().get(statementIndex);
        if (statement instanceof Container) {
            return ((Container)statement).getNext(FIRST_INDEX);
        } else {
            return statement;
        }
    }

    public Milestone getFinalMilestone() {
        return finalMilestone;
    }

    @Override
    public String toString() {
        return "Body{" + getContent() + "}";
    }

    /**
     * @see super#getType()
     */
    @Override
    public Type getType() {
        return Type.Body;
    }

    /**
     * Псевдо-оператор завершения процесса;
     * Добавляется в схему автоматически, т.е. его не нужно прописывать в тексте дескриптора
     */
    public static class End extends Statement {

        public End(long id) {
            super(id);
        }

        @Override
        public Type getType() {
            return Type.End;
        }
    }
}
