/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
package ru.manufacture.jaction.lang;

import java.util.List;

/**
 * условный оператор выбора (ветвления) процесса;
 * в отличие от оператора {@link ru.manufacture.jaction.lang.If} данный оператор
 * применим к нескольким возможным ситуациям (веткам процесса);
 * оператор последовательно проверяет условия {@link ru.manufacture.jaction.lang.Switch.Case} до тех пор,
 * пока проверка очередного условия не вернет положительный результат true или пока все условия не бутут проверены
 */
public class Switch extends Container {
    /**
     * оператор проверки выполнения условия
     */
    public static class Case extends Container.Nested {
        /**
         * условие для проверки
         */
        private Condition condition;

        /**
         * конструктор
         * @param id идентификатор оператора в рамках схемы процесса
         * @param condition условие для проверки
         * @param content массив дочерних операторов
         */
        public Case(long id, Condition condition, List<? extends Statement> content) {
            super(id, Type.Case, content);
            this.condition = condition;
            this.condition.setParent(this);
        }

        @Override
        public String toString() {
            return "Case{" + condition + " : " + getContent() + "}";
        }

        /**
         * @return возвращает условие для проверки
         */
        public Condition getCondition() {
            return condition;
        }
    }

    /**
     * конструктор
     * @param id идентификатор оператора в рамках схемы процесса
     * @param cases список операторов проверок условий
     */
    public Switch(long id, List<Case> cases) {
        super(id, cases);
    }

    /**
     * @see super#getNext(int)
     */
    @Override
    public Statement getNext(int statementIndex) {
        if (isEmpty()) {
            return getParent().getNext(getIndex() + 1);
        }
        if (!isEnterIndex(statementIndex)) {
            throw new IllegalStateException("Invalid statement index: " + statementIndex);
        }
        for (Statement statement : getContent()) {
            Case aCase = (Case) statement;
            if (aCase.getCondition().isTrue()) {
                return aCase.getNext(FIRST_INDEX);
            }
        }
        return getParent().getNext(getIndex() + 1);
    }

    /**
     * @see super#getType()
     */
    @Override
    public Type getType() {
        return Type.Switch;
    }

    /**
     * @return возвращает список операторов проверок условий
     */
    public List<Case> getCases() {
        return (List<Case>) getContent();
    }

    @Override
    public String toString() {
        return "Switch{" + getContent() + "} ";
    }
}
