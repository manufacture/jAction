/**
 * @author Degtyarev Roman
 * @date 04.03.2016.
 */
package ru.manufacture.jaction.lang;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import ru.manufacture.jaction.model.Milestone;

/**
 * оператор вызова процесса
 */
public class Call extends Statement {
    /**
     * имя процесса
     */
    private String process;
    /**
     * этап (контрольная точка) процесса
     */
    private Milestone milestone;
    /**
     * значения входных переменных контекста
     */
    private Map<String, String> input;
    /**
     * значения выходных переменных контекста
     */
    private Map<String, String> output;

    /**
     * конструктор
     *
     * @param id идентификатор оператора в рамках схемы процесса
     */
    public Call(long id, String process, Milestone milestone, Map<String, String> input, Map<String, String> output) {
        super(id);
        this.process = process;
        this.milestone = milestone;
        this.input = new ConcurrentHashMap<>(input);
        this.output = new ConcurrentHashMap<>(output);
    }

    /**
     * @return имя процесса
     */
    public String getProcess() {
        return process;
    }

    /**
     * @return этап (контрольная точка) процесса
     */
    public Milestone getMilestone() {
        return milestone;
    }

    /**
     * @return значения входных переменных контекста
     */
    public Map<String, String> getInput() {
        return input;
    }

    /**
     * @return значения выходных переменных контекста
     */
    public Map<String, String> getOutput() {
        return output;
    }

    /**
     * @return тип оператора
     */
    @Override
    public Type getType() {
        return Type.Call;
    }
}
