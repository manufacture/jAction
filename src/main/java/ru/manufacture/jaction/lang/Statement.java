/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
package ru.manufacture.jaction.lang;

/**
 * Оператор процесса
 */
public abstract class Statement {
    /**
     * тип оператора определяет ключевые выражения синтаксиса схемы процесса
     */
    public enum Type {
        Body, End, If, Then, Else, Execute, While, Switch, Case, Condition, Set, Inc, Fire, Call;

        /**
         * возвращает тип оператора по его строковому представлению игнорируя регистр строки
         * @param value имя оператора
         * @return тип оператора
         */
        public static Type of(String value) {
            for (Type type : values()) {
                if (type.name().equalsIgnoreCase(value))
                    return type;
            }
            throw new IllegalArgumentException("Statement type not supported: " + value);
        }
    }

    /**
     * идентификатор оператора в рамках схемы процесса
     */
    private long id;
    /**
     * индекс (порядковый номер) оператора в контейнере
     */
    private int index = 0;
    /**
     * контейнер, включающий в себя данный оператор
     */
    private Container parent;

    /**
     * конструктор
     * @param id идентификатор оператора в рамках схемы процесса
     */
    public Statement(long id) {
        this.id = id;
    }

    /**
     * @return идентификатор оператора в рамках схемы процесса
     */
    public long getId() {
        return id;
    }

    /**
     * @return тип оператора
     */
    public abstract Type getType();

    /**
     * устанавливает индекс (порядковый номер) оператора в контейнере
     * @param index
     */
    void setIndex(int index) {
        this.index = index;
    }

    /**
     * @return индекс (порядковый номер) оператора в контейнере
     */
    public int getIndex() {
        return index;
    }

    /**
     * устанавливает контейнер, включающий в себя данный оператор
     * @param parent родительский контейнер
     */
    void setParent(Container parent) {
        this.parent = parent;
    }

    /**
     * @return контейнер, включающий в себя данный оператор
     */
    public Container getParent() {
        return parent;
    }
}
