/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
package ru.manufacture.jaction.lang;

import ru.manufacture.jaction.model.ProcessInstance;
import ru.manufacture.jaction.protocol.Command;

/**
 * условие операторов ветвления
 */
public class Condition extends Statement {
    /**
     * текст скрипта - выражение условия, результатом вычисления которого должно быть булевское true или false
     */
    private String script;

    /**
     * конструктор условия
     * @param id идентификатор в рамках схемы процесса
     * @param script текст скрипта - выражение условия, результатом вычисления которого должно быть булевское true или false
     */
    public Condition(long id, String script) {
        super(id);
        this.script = script;
    }

    /**
     * @return текст скрипта - выражение условия
     */
    public String getScript() {
        return script;
    }

    /**
     * проверяет выполняется ли условие; выполняется скрипт, результат которого возвращается
     * @return true - если условие выполняется
     */
    public boolean isTrue() {
        Command command = Command.currentCommand();
        ProcessInstance processInstance = command.getProcessInstance();
        return command.getAlgorithmEngine().executeBooleanStatement(script, processInstance);
    }

    @Override
    public String toString() {
        return "Condition{" +
                "script='" + script + '\'' +
                "} ";
    }

    /**
     * @see super#getType()
     */
    @Override
    public Type getType() {
        return Type.Condition;
    }
}
