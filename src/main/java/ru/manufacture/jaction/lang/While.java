/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
package ru.manufacture.jaction.lang;

import java.util.List;

/**
 * оператор цикла while повторяет блок операторов, пока значение управляющего условия истинно
 */
public class While extends Container {
    /**
     * управляющее условие для проверки
     */
    private Condition condition;

    /**
     * конструктор
     * @param id идентификатор оператора в рамках схемы процесса
     * @param condition управляющее условие для проверки
     * @param content список дочерних операторов
     */
    public While(long id, Condition condition, List<? extends Statement> content) {
        super(id, content);
        this.condition = condition;
        this.condition.setParent(this);
    }

    /**
     * @return возвращает управляющее условие для проверки
     */
    public Condition getCondition() {
        return condition;
    }

    @Override
    public String toString() {
        return "While{" + condition + " : " + getContent() + "}";
    }

    /**
     * @see super#getType()
     */
    @Override
    public Type getType() {
        return Type.While;
    }

    /**
     * @see super#getNext(int)
     */
    @Override
    public Statement getNext(int statementIndex) {
        if (isEmpty()) {
            return getParent().getNext(getIndex() + 1);
        }
        if (isEnterIndex(statementIndex) || isExitIndex(statementIndex)) {
            boolean trueCondition = getCondition().isTrue();
            if (!trueCondition) {
                return getParent().getNext(getIndex() + 1);
            }
            if (isExitIndex(statementIndex)) {
                statementIndex = FIRST_INDEX;
            }
        }
        Statement statement = getContent().get(statementIndex);
        if (statement instanceof Container) {
            return ((Container)statement).getNext(FIRST_INDEX);
        } else {
            return statement;
        }
    }
}
