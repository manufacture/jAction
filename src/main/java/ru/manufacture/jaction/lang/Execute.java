/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
package ru.manufacture.jaction.lang;

import ru.manufacture.jaction.model.Action;
import ru.manufacture.jaction.model.Milestone;

/**
 * Оператор выполнения работы
 */
public class Execute extends Statement {
    /**
     * работа(шаг процесса), которую нужно выполнить
     */
    private Action action;
    /**
     * этап (контрольная точка) процесса
     */
    private Milestone milestone;

    /**
     * конструктор
     * @param id идентификатор оператора в рамках схемы процесса
     * @param action работа(шаг процесса), которую нужно выполнить
     * @param milestone этап (контрольная точка) процесса
     */
    public Execute(long id, Action action, Milestone milestone) {
        super(id);
        this.action = action;
        this.milestone = milestone;
    }

    /**
     * @return возвращает работу процесса
     */
    public Action getAction() {
        return action;
    }

    /**
     * @return возвращает этап процесса
     */
    public Milestone getMilestone() {
        return milestone;
    }

    @Override
    public String toString() {
        return "Execute{" + action + ", " + milestone + "}";
    }

    /**
     * @see super#getType()
     */
    @Override
    public Type getType() {
        return Type.Execute;
    }
}
