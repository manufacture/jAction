/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
package ru.manufacture.jaction.lang;

import java.util.List;

/**
 * условный оператор ветвления процесса
 */
public class If extends Container {
    /**
     * оператор-контейнер определяющий ветку процесса при выполнении условия ветвления
     */
    public static class Then extends Container.Nested {
        /**
         * конструктор
         * @param id идентификатор оператора в рамках схемы процесса
         * @param statements массив дочерних операторов
         */
        public Then(long id, List<? extends Statement> statements) {
            super(id, Type.Then, statements);
        }

        @Override
        public String toString() {
            return "Then{" + getContent() + "} ";
        }
    }

    /**
     * оператор-контейнер определяющий альтернативную ветку процесса, если условие ветвления не выполняется
     */
    public static class Else extends Container.Nested {
        /**
         * конструктор
         * @param id идентификатор оператора в рамках схемы процесса
         * @param statements массив дочерних операторов
         */
        public Else(long id, List<? extends Statement> statements) {
            super(id, Type.Else, statements);
        }

        @Override
        public String toString() {
            return "Else{" + getContent() + "} ";
        }
    }

    /**
     * условие ветвления
     */
    private Condition _condition;
    /**
     * ветка процесса при выполнении условия
     */
    private Then _then;
    /**
     * альтернативная ветка процесса, если условие не выполняется
     */
    private Else _else;

    /**
     * конструктор
     * @param id идентификатор оператора в рамках схемы процесса
     * @param _condition условие ветвления
     * @param _then ветка процесса при выполнении условия
     * @param _else альтернативная ветка процесса, если условие не выполняется
     */
    public If(long id, Condition _condition, Then _then, Else _else) {
        super(id, _then, _else);
        this._condition = _condition;
        this._condition.setParent(this);
        this._then = _then;
        this._else = _else;
    }

    /**
     * @see super#getNext(int)
     */
    @Override
    public Statement getNext(int statementIndex) {
        if (isEmpty()) {
            return getParent().getNext(getIndex() + 1);
        }
        if (!isEnterIndex(statementIndex)) {
            throw new IllegalStateException("Invalid statement index: " + statementIndex);
        }
        if (getCondition().isTrue())
            return _then.getNext(FIRST_INDEX);
        else
            return _else.getNext(FIRST_INDEX);
    }

    /**
     * @see super#getType()
     */
    @Override
    public Type getType() {
        return Type.If;
    }

    /**
     * @return возвращает условие ветвления
     */
    public Condition getCondition() {
        return _condition;
    }

    /**
     * @return возвращает ветку процесса при выполнении условия
     */
    public Then getThen() {
        return _then;
    }

    /**
     * @return возвращает альтернативную ветку процесса, если условие не выполняется
     */
    public Else getElse() {
        return _else;
    }

    @Override
    public String toString() {
        return "if{" + _condition + "}\n" + _then + "\n" + _else + "\n";
    }
}
