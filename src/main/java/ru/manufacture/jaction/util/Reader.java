package ru.manufacture.jaction.util;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import ru.manufacture.jaction.lang.Body;
import ru.manufacture.jaction.lang.Call;
import ru.manufacture.jaction.lang.Condition;
import ru.manufacture.jaction.lang.Execute;
import ru.manufacture.jaction.lang.FireEvent;
import ru.manufacture.jaction.lang.If;
import ru.manufacture.jaction.lang.IncValue;
import ru.manufacture.jaction.lang.SetValue;
import ru.manufacture.jaction.lang.Statement;
import ru.manufacture.jaction.lang.Switch;
import ru.manufacture.jaction.lang.While;
import ru.manufacture.jaction.model.Action;
import ru.manufacture.jaction.model.Algorithm;
import ru.manufacture.jaction.model.Declaration;
import ru.manufacture.jaction.model.Event;
import ru.manufacture.jaction.model.EventListener;
import ru.manufacture.jaction.model.Milestone;
import ru.manufacture.jaction.model.ProcessDescriptor;
import ru.manufacture.jaction.model.Role;
import ru.manufacture.jaction.model.Var;
import ru.manufacture.jaction.support.ReadProcessDeclarationException;
import ru.manufacture.persist.Declarable;
import ru.manufacture.types.Type;
import ru.manufacture.types.format.Format;
import ru.manufacture.util.Sequence;
import ru.manufacture.util.io.Xml;


import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 22.06.2015.
 */
public class Reader {
    private Sequence sequence;
    private Map<Long, Statement> statements;

    public Declaration read(Document xml) {
        Element element = xml.getRootElement();
        Declaration.Type type = Declaration.Type.of(element.getName());
        switch (type) {
            case Process: return readProcess(xml);
            case Declare: return readDeclaration(element, Collections.EMPTY_MAP);
            case Algorithms: return declare(readAlgorithms(element));
            case Roles: return declare(readRoles(element));
            case Milestones: return declare(readMilestones(element));
            case Events: return declare(readEvents(element));
            case EventListeners: return declare(readListeners(element));
        }
        return Declaration.Empty;
    }

    private Declaration declare(Collection<? extends Declarable> data) {
        return new Declaration(data, Collections.EMPTY_MAP);
    }

    private ProcessDescriptor readProcess(Document xml) {
        statements = new ConcurrentHashMap<>();
        sequence = new Sequence();
        Element process = xml.getRootElement();
        Map<String, Declaration> imports = readImports(process);
        Declaration declaration = readDeclaration(process.element("declare"), imports);
        Body body = readBody(process, declaration);
        ProcessDescriptor descriptor = new ProcessDescriptor(declaration);
        descriptor.setName(process.attributeValue("name"));
        descriptor.setDescription(process.attributeValue("description"));
        descriptor.setBody(body);
        descriptor.setStatements(statements);
        descriptor.setSource(Format.instance(Document.class).format(xml));
        descriptor.setProcessDispatcher(read(xml, "/*[name()='process']/*[name()='declare']/*[name()='actions']/@dispatcher"));
        descriptor.setAlgorithmEngine(read(xml, "/*[name()='process']/*[name()='declare']/*[name()='algorithms']/@engine"));
        descriptor.setProcessHook(read(xml, "/*[name()='process']/@hook"));
        return descriptor;
    }

    private String read(Document xml, String xPath) {
        Node node = xml.selectSingleNode(xPath);
        return node == null ? null : node.getText();
    }

    private Map<String, Declaration> readImports(Element process) {
        List<Element> imports = process.elements("import");
        if (imports.isEmpty()) {
            return Collections.emptyMap();
        }
        Map<String, Declaration> map = new HashMap<>();
        for (Element element : imports) {
            String resource = element.attributeValue("resource");
            if (!isBlank(resource)) {
                map.put(resource, readImport(resource));
            }
        }
        return map;
    }

    private Declaration readImport(String resourcePath) {
        try {
            ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            Resource resource = resolver.getResource(resourcePath);
            if (resource.exists()) {
                return read(Xml.read(resource.getInputStream()));
            }
            Path path = Paths.get(resourcePath);
            if (Files.exists(path)) {
                return read(Xml.read(path));
            }
            throw new FileNotFoundException("Resource not found: " + resourcePath);
        } catch (Exception e) {
            throw new ReadProcessDeclarationException("Can't read resource: " + resourcePath, e);
        }
    }

    private Declaration readDeclaration(Element declare, Map<String, Declaration> imports) {
        if (null == declare) {
            return imports.isEmpty() ? Declaration.Empty : new Declaration(Collections.EMPTY_LIST, imports);
        }
        Declaration declaration = new Declaration(imports);
        declaration.add(readAlgorithms(declare.element("algorithms")));
        declaration.add(readRoles(declare.element("roles")));
        declaration.add(readVars(declare.element("context")));
        declaration.add(readActions(declare.element("actions"), declaration));
        declaration.add(readMilestones(declare.element("milestones")));
        declaration.add(readEvents(declare.element("events")));
        declaration.add(readListeners(declare.element("event-listeners")));
        return declaration;
    }

    private List<Algorithm> readAlgorithms(Element algorithms) {
        List<Algorithm> list = new LinkedList<>();
        if (null == algorithms) return list;
        for (Object node : algorithms.elements("algorithm")) {
            Element algorithm = (Element) node;
            String name = algorithm.attributeValue("name");
            String descr = algorithm.attributeValue("description");
            String type = algorithm.attributeValue("type");
            String source = algorithm.elementTextTrim("source");
            String sourceType = elementAttribute(algorithm, "source", "type");
            Element viewElement = algorithm.element("view");
            String view = null;
            String viewType = null;
            if (null != viewElement) {
                view = viewElement.getTextTrim();
                viewType = viewElement.attributeValue("type");
            }
            Element params = algorithm.element("params");
            Algorithm alg = new Algorithm();
            alg.setName(name);
            alg.setDescription(descr);
            if (!isBlank(type)) {
                alg.setType(Algorithm.Type.valueOf(type.toUpperCase()));
            }
            if (!isBlank(viewType)) {
                alg.setViewType(Algorithm.ViewType.valueOf(viewType.toUpperCase()));
            }
            alg.setSource(source);
            if (!isBlank(sourceType)) {
                alg.setSourceType(Algorithm.SourceType.valueOf(sourceType.toUpperCase()));
            }
            alg.setView(view);
            if (null != params) {
                alg.setParams(readParams(params));
            }
            list.add(alg);
        }
        return list;
    }

    private String elementAttribute(Element parent, String element, String attribute) {
        if (null != parent) {
            Element child = parent.element(element);
            if (null != child) {
                return child.attributeValue(attribute);
            }
        }
        return null;
    }

    private List<EventListener> readListeners(Element listeners) {
        List<EventListener> list = new LinkedList<>();
        if (listeners == null) return list;
        for (Object node : listeners.elements("listener")) {
            Element listener = (Element) node;
            String name = listener.attributeValue("name");
            String descr = listener.attributeValue("description");
            String listen = listener.attributeValue("listen");
            String[] events = listen.split(",");
            EventListener el = new EventListener();
            el.setName(name);
            el.setDescription(descr);
            el.setListen(new HashSet<>(Arrays.asList(events)));
            list.add(el);
        }
        return list;
    }

    private List<Event> readEvents(Element events) {
        List<Event> list = new LinkedList<>();
        if (events == null) return list;
        for (Object node : events.elements("event")) {
            Element event = (Element) node;
            String name = event.attributeValue("name");
            String descr = event.attributeValue("description");
            Map<String, String> params = readParams(event);
            Event e = new Event();
            e.setName(name);
            e.setDescription(descr);
            e.setParams(params);
            list.add(e);
        }
        return list;
    }

    private Map<String, String> readParams(Element params) {
        Map<String, String> map = new HashMap<>();
        for (Object node : params.elements()) {
            Element param = (Element) node;
            map.put(param.getName(), param.getTextTrim());
        }
        return map;
    }

    private List<Milestone> readMilestones(Element milestones) {
        List<Milestone> list = new LinkedList<>();
        if (milestones == null) return list;
        for (Object node : milestones.elements("milestone")) {
            Element milestone = (Element) node;
            String name = milestone.attributeValue("name");
            String descr = milestone.attributeValue("description");
            Milestone m = new Milestone();
            m.setName(name);
            m.setDescription(descr);
            list.add(m);
        }
        return list;
    }

    private List<Action> readActions(Element actions, Declaration declaration) {
        List<Action> list = new ArrayList<>();
        if (actions == null) return list;
        for (Object node : actions.elements("action")) {
            Element action = (Element) node;
            String name = action.attributeValue("name");
            String descr = action.attributeValue("description");
            String roleName = action.attributeValue("role");
            String algorithmName = action.attributeValue("algorithm");
            Algorithm algorithm = null != algorithmName ? declaration.get(Algorithm.class, algorithmName) : null;
            Role role = declaration.get(Role.class, roleName);
            if (null == role) {
                throw new IllegalStateException("Action[" + name + "] role not found: " + roleName);
            }
            Action a = new Action();
            a.setName(name);
            a.setDescription(descr);
            a.setRole(role);
            a.setAlgorithm(algorithm);
            list.add(a);
        }
        return list;
    }

    private List<Role> readRoles(Element roles) {
        List<Role> list = new LinkedList<>();
        if (null == roles) return list;
        for (Object node : roles.elements("role")) {
            Element role = (Element) node;
            String name = role.attributeValue("name");
            String descr = role.attributeValue("description");
            Role.Type type = Role.Type.of(role.attributeValue("type"));
            Role r = new Role();
            r.setName(name);
            r.setDescription(descr);
            r.setType(type);
            list.add(r);
        }
        return list;
    }

    private List<Var> readVars(Element context) {
        List<Var> vars = new LinkedList<>();
        if (context == null) return vars;
        for (Object node : context.elements("var")) {
            Element var = (Element) node;
            String name = var.attributeValue("name");
            String descr = var.attributeValue("description");
            Type type = Type.of(var.attributeValue("type"));
            String defaultValue = var.attributeValue("default");
            Var v = new Var();
            v.setName(name);
            v.setDescription(descr);
            v.setType(type);
            v.setDefaultValue(defaultValue);
            vars.add(v);
        }
        return vars;
    }

    private Body readBody(Element process, Declaration declaration) {
        List<Statement> content = new ArrayList<Statement>();
        Element body = process.element("body");
        for (Object node : body.elements()) {
            content.add(read((Element) node, declaration));
        }
        Body.End end = new Body.End(nextId());
        content.add(end);
        register(end);
        String finalMilestone = body.attributeValue("final-milestone");
        Milestone milestone = null;
        if (!isBlank(finalMilestone)) {
            milestone = declaration.get(Milestone.class, finalMilestone);
        }
        return new Body(nextId(), content, milestone);
    }

    protected long nextId() {
        return sequence.getNext();
    }

    private Statement read(Element element, Declaration declaration) {
        Statement statement;
        switch (Statement.Type.of(element.getName())) {
            case Execute: statement = readExecute(element, declaration); break;
            case If: statement = readIf(element, declaration); break;
            case Then: statement = readThen(element, declaration); break;
            case Else: statement = readElse(element, declaration); break;
            case Switch: statement = readSwitch(element, declaration); break;
            case Case: statement = readCase(element, declaration); break;
            case While: statement = readWhile(element, declaration); break;
            case Condition: statement = readCondition(element, declaration); break;
            case Set: statement = readSetValue(element, declaration); break;
            case Inc: statement = readIncValue(element, declaration); break;
            case Fire: statement = readFireEvent(element, declaration); break;
            case Call: statement = readCall(element, declaration); break;
            default: throw new IllegalArgumentException("Statement type not supported: " + element.getName());
        }
        element.addAttribute("statement_num", String.valueOf(statement.getId()));
        return statement;
    }

    private Statement readCall(Element element, Declaration declaration) {
        String process = element.attributeValue("process");
        String milestoneName = element.attributeValue("milestone");
        if (isBlank(process)) {
            throw new IllegalStateException("Call process name required");
        }
        Milestone milestone = null;
        if (!isBlank(milestoneName)) {
            milestone = declaration.get(Milestone.class, milestoneName);
        }
        Call call = new Call(
                nextId(),
                process,
                milestone,
                readCallInput(element.elements("input")),
                readCallInput(element.elements("output"))
        );
        register(call);
        return call;
    }

    private Map<String, String> readCallInput(List elements) {
        Map<String, String> input = new HashMap<>();
        if (null != elements && !elements.isEmpty())
            for (Object node : elements) {
                Element item = (Element) node;
                String varName = item.attributeValue("var");
                if (isBlank(varName)) {
                    throw new IllegalStateException("Var name is required: " + item.asXML());
                }
                input.put(varName, item.getTextTrim());
            }
        return input;
    }

    private FireEvent readFireEvent(Element element, Declaration declaration) {
        String name = element.attributeValue("event");
        Event event = declaration.get(Event.class, name);
        if (null == event) {
            throw new IllegalStateException("Event not declared: " + event);
        }
        Map<String, String> params = readParams(element);
        FireEvent fireEvent = new FireEvent(nextId(), event, params);
        register(fireEvent);
        return fireEvent;
    }

    private IncValue readIncValue(Element element, Declaration declaration) {
        String var = element.attributeValue("var");
        String value = element.attributeValue("value");
        Var declaredVar = declaration.get(Var.class, var);
        if (null == declaredVar) {
            throw new IllegalStateException("Variable not declared: " + var);
        }
        IncValue incValue = new IncValue(nextId(), declaredVar, value);
        register(incValue);
        return incValue;
    }

    private SetValue readSetValue(Element element, Declaration declaration) {
        String var = element.attributeValue("var");
        String value = element.attributeValue("value");
        Var declaredVar = declaration.get(Var.class, var);
        if (null == declaredVar) {
            throw new IllegalStateException("Variable not declared: " + var);
        }
        SetValue setValue = new SetValue(nextId(), declaredVar, value);
        register(setValue);
        return setValue;
    }

    private Condition readCondition(Element element, Declaration declaration) {
        return new Condition(nextId(), element.getTextTrim());
    }

    private While readWhile(Element element, Declaration declaration) {
        Condition condition = null;
        List<Statement> content = new ArrayList<Statement>();
        for (Object node : element.elements()) {
            Statement statement = read((Element) node, declaration);
            if (statement instanceof Condition) {
                condition = (Condition) statement;
            } else {
                content.add(statement);
            }
        }
        return new While(nextId(), condition, content);
    }

    private Switch.Case readCase(Element element, Declaration declaration) {
        Condition condition = null;
        List<Statement> content = new ArrayList<Statement>();
        for (Object node : element.elements()) {
            Statement statement = read((Element) node, declaration);
            if (statement instanceof Condition) {
                condition = (Condition) statement;
            } else {
                content.add(statement);
            }
        }
        return new Switch.Case(nextId(), condition, content);
    }

    private Switch readSwitch(Element element, Declaration declaration) {
        List<Switch.Case> cases = new LinkedList<Switch.Case>();
        for (Object node : element.elements()) {
            cases.add((Switch.Case) read((Element) node, declaration));
        }
        return new Switch(nextId(), cases);
    }

    private If.Else readElse(Element element, Declaration declaration) {
        List<Statement> statements = new ArrayList<Statement>();
        for (Object node : element.elements()) {
            statements.add(read((Element) node, declaration));
        }
        return new If.Else(nextId(), statements);
    }

    private If.Then readThen(Element element, Declaration declaration) {
        List<Statement> statements = new ArrayList<Statement>();
        for (Object node : element.elements()) {
            statements.add(read((Element) node, declaration));
        }
        return new If.Then(nextId(), statements);
    }

    private If readIf(Element element, Declaration declaration) {
        Condition condition = null;
        If.Then ifThen = null;
        If.Else ifElse = null;
        for (Object node : element.elements()) {
            Statement statement = read((Element) node, declaration);
            if (statement instanceof Condition) {
                if (null == condition) {
                    condition = (Condition) statement;
                } else {
                    throw new IllegalArgumentException("Invalid syntax error; second <if> <condition> found: " + element.asXML());
                }
            }
            if (statement instanceof If.Then) {
                if (null == ifThen) {
                    ifThen = (If.Then) statement;
                } else {
                    throw new IllegalArgumentException("Invalid syntax error; second <if> <then> found: " + element.asXML());
                }
            }
            if (statement instanceof If.Else) {
                if (null == ifElse) {
                    ifElse = (If.Else) statement;
                } else {
                    throw new IllegalArgumentException("Invalid syntax error; second <if> <else> found: " + element.asXML());
                }
            }
        }
        return new If(nextId(), condition, ifThen, ifElse);
    }

    private Execute readExecute(Element element, Declaration declaration) {
        String actionName = element.attributeValue("action");
        String milestoneName = element.attributeValue("milestone");
        if (isBlank(actionName)) {
            throw new IllegalArgumentException("Action attribute required: " + element.asXML());
        }
        Milestone milestone = null;
        if (!isBlank(milestoneName)) {
            milestone = declaration.get(Milestone.class, milestoneName);
        }
        Action action = declaration.get(Action.class, actionName);
        Execute execute = new Execute(nextId(), action, milestone);
        register(execute);
        return execute;
    }

    private void register(Statement statement) {
        statements.put(statement.getId(), statement);
    }
}
