package ru.manufacture.jaction.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.file.Path;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;
import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.xml.sax.SAXException;
import ru.manufacture.jaction.model.Declaration;
import ru.manufacture.jaction.support.InvalidProcessDeclarationException;
import ru.manufacture.jaction.support.ReadProcessDeclarationException;
import ru.manufacture.util.io.Io;
import ru.manufacture.util.io.Xml;

public class Source {
    public static final String DEFAULT_ENCODING = "UTF-8";

    private static Logger logger = LoggerFactory.getLogger(Source.class);

    private String xml;

    public Source(String descriptorText) {
        this.xml = descriptorText;
        Xsd.validate(this);
    }

    public Source(InputStream inputStream, String encoding) throws IOException {
        this(Io.read(inputStream, encoding));
    }

    public Source(Path pathTodescriptor, String encoding) throws IOException {
        this(Io.read(pathTodescriptor, encoding));
    }

    public String asXml() {
        return xml;
    }

    public java.io.Reader getReader() {
        return new StringReader(xml);
    }

    public Declaration read() {
        try {
            return new Reader().read(Xml.read(getReader()));
        } catch (DocumentException e) {
            throw new ReadProcessDeclarationException("Read process description fails", e);
        }
    }

    public static Source[] lookup(String sourcePattern) {
        return lookup(sourcePattern, DEFAULT_ENCODING);
    }

    public static Source[] lookup(String sourcePattern, String encoding) {
        try {
            ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            Resource[] resources = resolver.getResources(sourcePattern);
            Source[] sources = new Source[resources.length];
            for (int i = 0; i < resources.length; i++) {
                sources[i] = new Source(resources[i].getInputStream(), encoding);
            }
            return sources;
        } catch (IOException e) {
            throw new ReadProcessDeclarationException("Can't lookup descriptors: " + sourcePattern, e);
        }
    }

    public static class Xsd {
        private static Schema schema = read("classpath:META-INF/jAction/xsd/process.xsd");

        private static Schema read(String xsdPath) {
            ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            Resource resource = resolver.getResource(xsdPath);
            try (InputStream in = resource.getInputStream()) {
                javax.xml.transform.Source src = new StreamSource(in);
                return null != resource ? Xml.schema(src) : null;
            } catch (IOException | SAXException e) {
                logger.error("Can't read xml schema: " + xsdPath, e);
                return null;
            }
        }

        public static void validate(Source descriptor) {
            if (schema != null) {
                Validator validator = schema.newValidator();
                try {
                    validator.validate(new StreamSource(descriptor.getReader()));
                } catch (SAXException | IOException e) {
                    throw new InvalidProcessDeclarationException("Invalid process descriptor", e);
                }
            }
        }
    }
}