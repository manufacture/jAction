package ru.manufacture.jaction.protocol;

import java.util.ArrayDeque;
import java.util.Deque;
import ru.manufacture.jaction.model.ActionInstance;
import ru.manufacture.jaction.model.Algorithm;
import ru.manufacture.jaction.model.ProcessDescriptor;
import ru.manufacture.jaction.model.ProcessInstance;
import ru.manufacture.jaction.security.IUserCredential;
import ru.manufacture.jaction.support.ProcessService;
import ru.manufacture.jaction.support.algorithm.AlgorithmEngine;
import ru.manufacture.jaction.support.dispatch.ProcessDispatcher;
import ru.manufacture.jaction.support.hook.ProcessHook;

/**
 * @author Degtyarev Roman
 * @date 09.12.2015.
 */
public abstract class Command<Request, Response> {
    private static ThreadLocal<Deque<Command>> stackHolder = new ThreadLocal<>();

    private IUserCredential userCredential;
    private Request request;
    private ProcessService processService;
    private ProcessHook processHook;
    private ProcessDispatcher processDispatcher;
    private AlgorithmEngine algorithmEngine;

    public Command(IUserCredential userCredential, Request request) {
        this.userCredential = userCredential;
        this.request = request;
        this.processService = ProcessService.instance();
    }

    protected abstract Response doInvoke();

    public Response invoke() {
        Deque<Command> stack = stackHolder.get();
        try {
            if (null == stack) {
                stack = new ArrayDeque<>();
                stackHolder.set(stack);
            }
            stack.push(this);
            return doInvoke();
        } finally {
            stack.pop();
            if (stack.isEmpty()) {
                stackHolder.remove();
            }
        }
    }

    public static Command currentCommand() {
        Deque<Command> stack = stackHolder.get();
        return null == stack ? null : stack.peek();
    }

    public ProcessInstance getProcessInstance() {
        throw new IllegalStateException("Method not supported");
    }

    public ActionInstance getActionInstance() {
        throw new IllegalStateException("Method not supported");
    }

    public long getActionInstanceId() {
        throw new IllegalStateException("Method not supported");
    }

    public Request getRequest() {
        return request;
    }

    public IUserCredential getUserCredential() {
        return userCredential;
    }

    protected ProcessService getProcessService() {
        return processService;
    }

    public AlgorithmEngine getAlgorithmEngine() {
        if (this.algorithmEngine != null) {
            return this.algorithmEngine;
        }
        ProcessDescriptor descriptor = getProcessInstance().getProcess().descriptor();
        String engine = descriptor.getAlgorithmEngine();
        boolean isDefault = ProcessDescriptor.DEFAULT.equalsIgnoreCase(engine);
        this.algorithmEngine = isDefault ? AlgorithmEngine.instance() : (AlgorithmEngine) Algorithm.forName(engine).instance();
        return this.algorithmEngine;
    }

    public ProcessDispatcher getDispatcher() {
        if (this.processDispatcher != null) {
            return this.processDispatcher;
        }
        ProcessDescriptor descriptor = getProcessInstance().getProcess().descriptor();
        String dispatcher = descriptor.getProcessDispatcher();
        boolean isDefault = ProcessDescriptor.DEFAULT.equalsIgnoreCase(dispatcher);
        this.processDispatcher = isDefault ? ProcessDispatcher.instance() : (ProcessDispatcher) Algorithm.forName(dispatcher).instance();
        return this.processDispatcher;
    }

    public ProcessHook getProcessHook() {
        if (this.processHook != null) {
            return this.processHook;
        }
        ProcessDescriptor descriptor = getProcessInstance().getProcess().descriptor();
        String hook = descriptor.getProcessHook();
        boolean isDefault = ProcessDescriptor.DEFAULT.equalsIgnoreCase(hook);
        this.processHook = isDefault ? ProcessHook.instance() : (ProcessHook) Algorithm.forName(hook).instance();
        return this.processHook;
    }
}
