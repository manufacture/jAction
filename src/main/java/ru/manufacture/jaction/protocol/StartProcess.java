package ru.manufacture.jaction.protocol;

import java.util.Collections;
import java.util.Map;
import ru.manufacture.jaction.model.ActionInstance;
import ru.manufacture.jaction.model.ProcessInstance;
import ru.manufacture.jaction.model.Var;
import ru.manufacture.jaction.security.IUserCredential;

/**
 * @author Degtyarev Roman
 * @date 09.12.2015.
 */
public class StartProcess<Data> extends MoveProcess<Data> {
    private String processName;
    private ProcessInstance processInstance;
    private ActionInstance parentActionInstance;
    private Map<Var, Object> context;

    public StartProcess(IUserCredential userCredential, String processName, ProcessRequest<Data> request) {
        this(userCredential, processName, request, null, Collections.EMPTY_MAP);
    }

    public StartProcess(IUserCredential userCredential, String processName, ProcessRequest<Data> request, ActionInstance parentActionInstance, Map<Var, Object> context) {
        super(userCredential, request);
        this.processName = processName;
        this.context = context;
        this.parentActionInstance = parentActionInstance;
    }

    public String getProcessName() {
        return processName;
    }

    @Override
    public ProcessInstance getProcessInstance() {
        return processInstance;
    }

    public Map<Var, Object> getContext() {
        return context;
    }

    public ActionInstance getParentActionInstance() {
        return parentActionInstance;
    }

    @Override
    protected ProcessResponse<Data> doInvoke() {
        ru.manufacture.jaction.model.Process process = getProcessService().getProcess(processName);
        if (null == process) {
            throw new IllegalStateException("Process not found: " + processName);
        }
        processInstance = process.newInstance(parentActionInstance);
        initContext();
        return getNextActionInstance(processInstance, getRequest());
    }

    private void initContext() {
        getProcessHook().setContext(processInstance, context);
    }
}
