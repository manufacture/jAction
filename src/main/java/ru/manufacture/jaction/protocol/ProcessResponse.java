package ru.manufacture.jaction.protocol;

import ru.manufacture.jaction.model.ActionInstance;
import ru.manufacture.jaction.model.Algorithm;

/**
 * @author Degtyarev Roman
 * @date 20.08.2015.
 */
public class ProcessResponse <Data> {
    private Long actionId;
    private String view;
    private Algorithm.ViewType viewType;
    private Data data;

    public ProcessResponse(ActionInstance actionInstance) {
        this(actionInstance, null);
    }

    public ProcessResponse(ActionInstance actionInstance, Data data) {
        this.actionId = null;
        this.data = data;
        if (null != actionInstance && actionInstance != ActionInstance.NO_ACTION) {
            this.actionId = actionInstance.getId();
            Algorithm algorithm = actionInstance.getAction().getAlgorithm();
            if (null != algorithm) {
                this.view = algorithm.getView();
                this.viewType = algorithm.getViewType();
            }
        }
    }

    public Long getActionId() {
        return actionId;
    }

    public Algorithm.ViewType getViewType() {
        return viewType;
    }

    public String getView() {
        return view;
    }

    public boolean isActionAvailable() {
        return null != actionId && actionId > 0;
    }

    public Data getData() {
        return data;
    }

    @Override
    public String toString() {
        return "ProcessResponse{" +
                "actionId=" + actionId +
                ", view='" + view + '\'' +
                ", viewType=" + viewType +
                '}';
    }
}
