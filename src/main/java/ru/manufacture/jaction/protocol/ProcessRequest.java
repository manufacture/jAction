package ru.manufacture.jaction.protocol;

/**
 * @author Degtyarev Roman
 * @date 20.08.2015.
 */
public class ProcessRequest<Data> {
    private Data data;

    public ProcessRequest(Data data) {
        this.data = data;
    }

    public Data getData() {
        return data;
    }

    public static <Data> ProcessRequest<Data> currentRequest() {
        Command command = Command.currentCommand();
        if (null != command && command.getRequest() instanceof ProcessRequest)
            return (ProcessRequest<Data>) command.getRequest();
        else
            return null;
    }
}
