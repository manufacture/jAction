package ru.manufacture.jaction.protocol;

import ru.manufacture.jaction.model.ActionInstance;
import ru.manufacture.jaction.model.ProcessInstance;
import ru.manufacture.jaction.security.IUserCredential;
import ru.manufacture.jaction.support.IdentityLock;

/**
 * @author Degtyarev Roman
 * @date 09.12.2015.
 */
public class CompleteAction<Data> extends MoveProcess<Data> {
    private long actionInstanceId;

    public CompleteAction(IUserCredential userCredential, long actionInstanceId, ProcessRequest<Data> request) {
        super(userCredential, request);
        this.actionInstanceId = actionInstanceId;
    }

    @Override
    public long getActionInstanceId() {
        return actionInstanceId;
    }

    @Override
    public ActionInstance getActionInstance() {
        return getProcessService().loadActionInstance(actionInstanceId);
    }

    @Override
    public ProcessInstance getProcessInstance() {
        return getActionInstance().getProcessInstance();
    }

    @Override
    protected ProcessResponse<Data> doInvoke() {
        ActionInstance actionInstance = getActionInstance();
        if (isEmpty(actionInstance)) {
            return new ProcessResponse<>(ActionInstance.NO_ACTION);
        }
        IdentityLock.Value<Long> lock = lock(actionInstance);
        try {
            if (!acceptActionTouch(actionInstance)) {
                return new ProcessResponse<>(ActionInstance.NO_ACTION);
            }
            getAlgorithmEngine().completeAction(actionInstance, getRequest());
            closeActionInstance(actionInstance);
            return getNextActionInstance(actionInstance.getProcessInstance(), getRequest());
        } finally {
            release(lock);
        }
    }
}
