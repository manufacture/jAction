package ru.manufacture.jaction.protocol;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import ru.manufacture.jaction.dao.ProcessDao;
import ru.manufacture.jaction.lang.Call;
import ru.manufacture.jaction.lang.Execute;
import ru.manufacture.jaction.lang.Statement;
import ru.manufacture.jaction.model.Action;
import ru.manufacture.jaction.model.ActionInstance;
import ru.manufacture.jaction.model.Algorithm;
import ru.manufacture.jaction.model.Declaration;
import ru.manufacture.jaction.model.Group;
import ru.manufacture.jaction.model.Milestone;
import ru.manufacture.jaction.model.Process;
import ru.manufacture.jaction.model.ProcessInstance;
import ru.manufacture.jaction.model.Role;
import ru.manufacture.jaction.model.Var;
import ru.manufacture.jaction.security.IGroup;
import ru.manufacture.jaction.security.IRole;
import ru.manufacture.jaction.security.IUserCredential;
import ru.manufacture.jaction.support.IdentityLock;
import ru.manufacture.jaction.support.ProcessIterator;
import ru.manufacture.spring.Spring;

/**
 * @author Degtyarev Roman
 * @date 09.12.2015.
 */
public abstract class MoveProcess<Data> extends Command<ProcessRequest<Data>, ProcessResponse<Data>> {
    private static final IdentityLock<Long> synchronizator = new IdentityLock<>();

    public MoveProcess(IUserCredential userCredential, ProcessRequest<Data> request) {
        super(userCredential, request);
    }

    protected IdentityLock.Value<Long> lock(ActionInstance actionInstance) {
        return synchronizator.lock(actionInstance);
    }

    protected void release(IdentityLock.Value<Long> actionInstanceLock) {
        synchronizator.release(actionInstanceLock);
    }

    protected  ProcessResponse<Data> getNextActionInstance(ProcessInstance processInstance, ProcessRequest<Data> request) {
        ProcessIterator processIterator = new ProcessIterator(processInstance);
        Statement statement;
        while ((statement = processIterator.getNext()) != null) {
            switch (statement.getType()) {
                case Execute: {
                    ActionInstance actionInstance = createActionInstance(processInstance, ((Execute) statement).getAction(), ((Execute) statement).getMilestone());
                    Role role = actionInstance.getAction().getRole();
                    switch (role.getType()) {
                        case SYSTEM: {
                            getAlgorithmEngine().executeSystemAction(actionInstance, request);
                            closeActionInstance(actionInstance);
                            continue;
                        }
                        case HUMAN: {
                            /*ExecuteAction<Data> executeAction = new ExecuteAction<>(getUserCredential(), actionInstance.getId(), request);
                            return executeAction.invoke();*/
                            return new ProcessResponse<>(actionInstance);
                        }
                    }
                    throw new IllegalStateException("Role type not supported: " + role.getType());
                }
                case Call: {
                    Call call = (Call) statement;
                    Process process = getProcessService().getProcess(call.getProcess());
                    if (null == process) {
                        throw new IllegalStateException("Call process fails; Process not found: " + call.getProcess());
                    }
                    ActionInstance actionInstance = createActionInstance(processInstance, getCallAction(processInstance.getProcess()), call.getMilestone());
                    Map<Var, Object> input = getContext(processInstance, call.getInput(), process);
                    StartProcess<Data> startProcess = new StartProcess<>(getUserCredential(), process.getName(), getRequest(), actionInstance, input);
                    ProcessResponse<Data> response = startProcess.invoke();
                    if (!response.isActionAvailable()) {
                        closeActionInstance(actionInstance);
                        Map<Var, Object> output = getContext(startProcess.getProcessInstance(), call.getOutput(), processInstance.getProcess());
                        getProcessHook().setContext(processInstance, output);
                        continue;
                    }
                    return response;
                }
            }
        }
        ActionInstance parentActionInstance = processInstance.getParentActionInstance();
        if (null != parentActionInstance) {
            ProcessInstance parentProcessInstance = parentActionInstance.getProcessInstance();
            Call call = (Call) parentProcessInstance.getCurrent();
            Map<Var, Object> output = getContext(processInstance, call.getOutput(), parentProcessInstance.getProcess());
            getProcessHook().setContext(parentProcessInstance, output);
            return getNextActionInstance(parentProcessInstance, getRequest());
        } else {
            return new ProcessResponse<>(ActionInstance.NO_ACTION);
        }
    }

    private Action getCallAction(Process process) {
        ProcessDao processDao = Spring.bean("processDao");
        Action action = processDao.getAction(process, Action.CALL_PROCESS);
        if (null == action) {
            action = new Action();
            action.setProcess(process);
            action.setName(Action.CALL_PROCESS);
            action.setRole(Role.system());
            action.setAlgorithm(Algorithm.empty());
            processDao.saveAction(action);
        }
        return action;
    }

    private Map<Var, Object> getContext(ProcessInstance processInstance, Map<String, String> input, Process process) {
        if (null != input && !input.isEmpty()) {
            Map<Var, Object> context = new HashMap<>();
            Declaration declaration = process.getDeclaration();
            for (Map.Entry<String, String> entry : input.entrySet()) {
                Var var = declaration.get(Var.class, entry.getKey());
                if (null == var) {
                    throw new IllegalStateException("Process[" + process.getName() + "] variable not declared: " + entry.getKey());
                }
                context.put(var, processInstance.getVar(entry.getValue()));
            }
            return context;
        }
        return Collections.EMPTY_MAP;
    }

    public void closeActionInstance(ActionInstance actionInstance) {
        actionInstance.setCompleteDate(new Date());
        actionInstance.setCompleteUser(getUserCredential().getLogin());
        actionInstance.setStatus(ActionInstance.Status.COMPLETE);
        getProcessService().saveActionInstance(actionInstance);
    }

    public ActionInstance createActionInstance(ProcessInstance processInstance, Action action, Milestone milestone) {
        if (null != milestone) {
            milestone = getProcessService().getMilestone(milestone.getName());
            processInstance.setMilestone(milestone);
            getProcessService().saveProcessInstance(processInstance);
        }
        ActionInstance actionInstance = new ActionInstance();
        actionInstance.setMilestone(milestone);
        actionInstance.setAction(getProcessService().getAction(processInstance.getProcess(), action.getName()));
        actionInstance.setCreateDate(new Date());
        actionInstance.setStatus(ActionInstance.Status.ACTIVE);
        actionInstance.setCreateUser(getUserCredential().getLogin());
        actionInstance.setProcessInstance(processInstance);
        String description = getAlgorithmEngine().createDescription(actionInstance);
        actionInstance.setDescription(description);
        IGroup group = getDispatcher().dispatch(actionInstance);
        actionInstance.setGroup(getProcessService().getOrCreateGroup(group.getName()));
        getProcessService().saveActionInstance(actionInstance);
        return actionInstance;
    }

    protected boolean acceptActionTouch(ActionInstance actionInstance) {
        if (isEmpty(actionInstance)) {
            return false;
        }
        if (!actionInstance.isActive()) {
            return false;
        }
        return acceptUserPermissions(actionInstance);
    }

    protected boolean acceptUserPermissions(ActionInstance actionInstance) {
        Role role = actionInstance.getAction().getRole();
        IGroup group = actionInstance.getGroup();
        return acceptUserRole(role) && acceptUserGroup(group);
    }

    protected boolean acceptUserGroup(IGroup group) {
        if (null == group || Group.ANY.equals(group.getName())) {
            return true;
        }
        for (IGroup userGroup : getUserCredential().getGroups()) {
            if (userGroup.getName().equals(group.getName())) {
                return true;
            }
        }
        return false;
    }

    protected boolean acceptUserRole(IRole role) {
        for (IRole userRole : getUserCredential().getRoles()) {
            if (userRole.getName().equals(role.getName())) {
                return true;
            }
        }
        return false;
    }

    protected boolean isEmpty(ActionInstance actionInstance) {
        return actionInstance == ActionInstance.NO_ACTION || null == actionInstance;
    }
}
