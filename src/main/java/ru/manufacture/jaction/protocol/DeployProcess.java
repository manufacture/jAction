package ru.manufacture.jaction.protocol;

import java.util.LinkedList;
import java.util.List;
import ru.manufacture.jaction.model.Declaration;
import ru.manufacture.jaction.model.ProcessDescriptor;
import ru.manufacture.jaction.security.IUserCredential;
import ru.manufacture.jaction.util.Source;

/**
 * @date 29.07.2016.
 */
public class DeployProcess extends Command<Source[], String[]> {
    public DeployProcess(IUserCredential userCredential, String sourcePattern) {
        this(userCredential, Source.lookup(sourcePattern));
    }

    public DeployProcess(IUserCredential userCredential, Source ... source) {
        super(userCredential, source);
    }

    @Override
    protected String[] doInvoke() {
        List<ProcessDescriptor> processes = new LinkedList<>();
        for (Source source : getRequest()) {
            Declaration declaration = source.read();
            if (declaration instanceof ProcessDescriptor) {
                processes.add((ProcessDescriptor) declaration);
            } else {
                getProcessService().deploy(declaration);
            }
        }
        String [] names = new String[processes.size()];
        int i = 0;
        for (ProcessDescriptor process : processes) {
            ru.manufacture.jaction.model.Process deployProcess = getProcessService().deployProcess(process);
            names[i++] = deployProcess.getName();
        }
        return names;
    }
}
