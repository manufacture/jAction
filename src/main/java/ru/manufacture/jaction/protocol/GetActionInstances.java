package ru.manufacture.jaction.protocol;

import ru.manufacture.jaction.model.view.ActionInstanceFilter;
import ru.manufacture.jaction.model.view.VActionInstance;
import ru.manufacture.jaction.security.IUserCredential;
import ru.manufacture.util.pageable.SimplePageable;

/**
 * @author Degtyarev Roman
 * @date 09.12.2015.
 */
public class GetActionInstances extends Command<ActionInstanceFilter, SimplePageable<VActionInstance>> {
    public GetActionInstances(IUserCredential userCredential, ActionInstanceFilter filter) {
        super(userCredential, filter);
    }

    @Override
    protected SimplePageable<VActionInstance> doInvoke() {
        return getProcessService().getActionInstances(getUserCredential(), getRequest());
    }
}
