package ru.manufacture.jaction.model;

import ru.manufacture.persist.Declarable;
import ru.manufacture.types.Type;

/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
public class Var extends Declarable {
    private Type type;
    private String defaultValue;

    public void setType(Type type) {
        this.type = type;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Type getType() {
        return type;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public String toString() {
        return "{" + getName() + ":" + type + " [default='" + defaultValue + "']}";
    }
}
