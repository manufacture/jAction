package ru.manufacture.jaction.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import ru.manufacture.jaction.lang.Statement;
import ru.manufacture.jaction.support.ProcessService;
import ru.manufacture.persist.Identity;

/**
 * @author Degtyarev Roman
 * @date 21.07.2015.
 */
@Entity
@Table(name = "J_PROCESS_INSTANCE")
public class ProcessInstance implements Serializable, Identity<Long> {
    public enum Status {ACTIVE, COMPLETE, TERMINATE, SLEEP}

    private long id;
    private Process process;
    private Milestone milestone;
    private Status status;
    private Date createDate;
    private Date completeDate;
    private Long statementId;
    private ActionInstance parentActionInstance;

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="jaction_seq")
    @SequenceGenerator(name="jaction_seq", sequenceName="jaction_seq", allocationSize = 1)
    public Long getId() {
        return id;
    }

    @Column(name = "create_date", nullable = false)
    public Date getCreateDate() {
        return createDate;
    }

    @Column(name = "complete_date")
    public Date getCompleteDate() {
        return completeDate;
    }

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    public Status getStatus() {
        return status;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "milestone_id")
    public Milestone getMilestone() {
        return milestone;
    }

    @Column(name = "statement_num")
    public Long getStatementId() {
        return statementId;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "process_id", nullable = false)
    public Process getProcess() {
        return process;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_actioninstance_id")
    public ActionInstance getParentActionInstance() {
        return parentActionInstance;
    }

    public void setParentActionInstance(ActionInstance parentActionInstance) {
        this.parentActionInstance = parentActionInstance;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setMilestone(Milestone milestone) {
        this.milestone = milestone;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public void setCompleteDate(Date completeDate) {
        this.completeDate = completeDate;
    }

    public void setStatementId(Long statementId) {
        this.statementId = statementId;
    }

    @Transient
    public Statement getCurrent() {
        if (null == statementId) {
            throw new IllegalStateException("No current statement initialized");
        }
        return process.getStatement(statementId);
    }

    public <Type> Type getVar(String varName) {
        Var var = getVarDeclaration(varName);
        Type value = ProcessService.instance().loadVariable(this, var);
        return null == value ? (Type) var.getType().cast(var.getDefaultValue()) : value;
    }

    public void setVar(String varName, Object value) {
        Var var = getVarDeclaration(varName);
        ProcessService.instance().saveVariable(this, var, value);
    }

    private Var getVarDeclaration(String varName) {
        Var var = getProcess().getDeclaration().get(Var.class, varName);
        if (null == var) {
            throw new IllegalStateException("Variable not declared: " + varName);
        }
        return var;
    }
}
