/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
package ru.manufacture.jaction.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import ru.manufacture.persist.Declarable;

public class DeclarationTest implements Serializable {

    public static DeclarationTest empty() {
        return new DeclarationTest(null, null, null, null, null, null, null);
    }

    private Map<String, Var> vars;
    private Map<String, Role> roles;
    private Map<String, Action> actions;
    private Map<String, Milestone> milestones;
    private Map<String, Event> events;
    private Map<String, EventListener> listeners;
    private final Map<String, Algorithm> algorithms;

    public DeclarationTest(List<Var> vars,
                       List<Role> roles,
                       List<Action> actions,
                       List<Milestone> milestones,
                       List<Event> events,
                       List<EventListener> listeners,
                       List<Algorithm> algorithms) {
        this.vars = toMap(vars);
        this.roles = toMap(roles);
        this.actions = toMap(actions);
        this.milestones = toMap(milestones);
        this.events = toMap(events);
        this.listeners = toMap(listeners);
        this.algorithms = toMap(algorithms);
    }

    public static  <T extends Declarable> Map<String, T> toMap(List<T> list) {
        Map<String, T> map = new ConcurrentHashMap<String, T>();
        if (null == list) return map;
        for (T t : list) {
            map.put(t.getName(), t);
        }
        return map;
    }

    public Map<String, Var> getVars() {
        return vars;
    }

    public Map<String, Role> getRoles() {
        return roles;
    }

    public Map<String, Action> getActions() {
        return actions;
    }

    public Map<String, Milestone> getMilestones() {
        return milestones;
    }

    public Map<String, Event> getEvents() {
        return events;
    }

    public Map<String, EventListener> getListeners() {
        return listeners;
    }

    public Map<String, Algorithm> getAlgorithms() {
        return algorithms;
    }

    public static class Import {
        private String resource;
        private Map<ImportKey, Declarable> data;

        public Import(String resource, Collection<Declarable> data) {
            this.resource = resource;
            this.data = new ConcurrentHashMap<>();
            for (Declarable declarable : data) {
                ImportKey key = new ImportKey(declarable.getClass(), declarable.getName());
                this.data.put(key, declarable);
            }
        }

        public Declarable get(Class<? extends Declarable> type, String declarableName) {
            ImportKey key = new ImportKey(type, declarableName);
            return data.get(key);
        }
    }

    private static class ImportKey {
        Class<? extends Declarable> type;
        String name;

        public ImportKey(Class<? extends Declarable> type, String name) {
            this.type = type;
            this.name = name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ImportKey importKey = (ImportKey) o;

            if (!name.equals(importKey.name)) return false;
            if (!type.equals(importKey.type)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = type.hashCode();
            result = 31 * result + name.hashCode();
            return result;
        }
    }
}
