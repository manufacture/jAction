package ru.manufacture.jaction.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import ru.manufacture.persist.Identity;
import ru.manufacture.types.Type;

/**
 * @author Degtyarev Roman
 * @date 27.08.2015.
 */
@Entity
@Table(name = "J_VAR_VALUE")
public class VarValue implements Serializable, Identity<Long> {
    private long id;
    private ProcessInstance processInstance;
    private Date processDate;
    private Type type;
    private String name;
    private String value;

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="jaction_seq")
    @SequenceGenerator(name="jaction_seq", sequenceName="jaction_seq", allocationSize = 1)
    public Long getId() {
        return id;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "pi_id", nullable = false)
    public ProcessInstance getProcessInstance() {
        return processInstance;
    }

    @Column(name = "process_date", nullable = false)
    public Date getProcessDate() {
        return processDate;
    }

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    public Type getType() {
        return type;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    @Column(name = "value", length = 1024)
    public String getValue() {
        return value;
    }

    public void set(Object value) {
        this.value = type.toString(value);
    }

    @Transient
    public Object get() {
        return type.cast(value);
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setProcessInstance(ProcessInstance processInstance) {
        this.processInstance = processInstance;
    }

    public void setProcessDate(Date processDate) {
        this.processDate = processDate;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
