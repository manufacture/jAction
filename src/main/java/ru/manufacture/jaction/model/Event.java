package ru.manufacture.jaction.model;

import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;
import ru.manufacture.persist.Declarable;
import ru.manufacture.types.format.Format;

/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
@Entity
@Table(name = "J_EVENT")
public class Event extends Declarable {
    private String paramsAsString;
    private Map<String, String> params;

    @Lob
    @Column(name = "params")
    public String getParamsAsString() {
        return paramsAsString;
    }

    public void setParamsAsString(String paramsAsString) {
        this.paramsAsString = paramsAsString;
        this.params = Format.toObject(Map.class, paramsAsString);
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
        this.paramsAsString = Format.instance(Map.class).format(params);
    }

    @Transient
    public Map<String, String> getParams() {
        return params;
    }

    @Override
    public String toString() {
        return "Event[" + getName() + "], params" + params;
    }
}
