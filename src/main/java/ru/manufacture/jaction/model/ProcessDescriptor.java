package ru.manufacture.jaction.model;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import ru.manufacture.jaction.lang.Body;
import ru.manufacture.jaction.lang.Statement;
import ru.manufacture.jaction.util.Source;


import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 26.08.2015.
 */
public class ProcessDescriptor extends Declaration implements Serializable {
    public static final String DEFAULT = "default";

    private String source;
    private Body body;
    private Map<Long, ? extends Statement> statements;
    private String processDispatcher;
    private String algorithmEngine;
    private String processHook;

    public ProcessDescriptor(Declaration declaration) {
        super(declaration);
    }

    public void setProcessDispatcher(String beanName) {
        this.processDispatcher = beanName;
    }

    public void setAlgorithmEngine(String beanName) {
        this.algorithmEngine = beanName;
    }

    public void setProcessHook(String beanName) {
        this.processHook = beanName;
    }

    public String getProcessDispatcher() {
        return isBlank(processDispatcher) ? DEFAULT : processDispatcher;
    }

    public String getAlgorithmEngine() {
        return isBlank(algorithmEngine) ? DEFAULT : algorithmEngine;
    }

    public String getProcessHook() {
        return isBlank(processHook) ? DEFAULT : processHook;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Map<Long, ? extends Statement> getStatements() {
        return statements;
    }

    public void setStatements(Map<Long, ? extends Statement> statements) {
        this.statements = statements;
    }

    private static class Cache {
        static Map<String, ProcessDescriptor> pool = new ConcurrentHashMap<>();
    }

    public static ProcessDescriptor get(String processName, String descriptorText) {
        ProcessDescriptor descriptor = Cache.pool.get(processName);
        if (null == descriptor) {
            Source source = new Source(descriptorText);
            descriptor = (ProcessDescriptor) source.read();
            Cache.pool.put(processName, descriptor);
        }
        return descriptor;
    }

    public static void resetCache() {
        Cache.pool.clear();
    }
}
