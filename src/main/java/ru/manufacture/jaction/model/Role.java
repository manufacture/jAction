package ru.manufacture.jaction.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import ru.manufacture.jaction.security.IGroup;
import ru.manufacture.jaction.security.IRole;
import ru.manufacture.persist.Declarable;
import ru.manufacture.persist.DeclarableDao;
import ru.manufacture.spring.Spring;

/**
 * @author Degtyarev Roman
 * @date 31.07.2015.
 */
@Entity
@Table(name = "J_ROLE")
public class Role extends Declarable implements IRole {
    private IRole.Type type;

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    public Type getType() {
        return type;
    }

    public static final Role forName(String name) {
        DeclarableDao declarableDao = Spring.bean("declarableDao");
        return declarableDao.getDeclarable(Role.class, name);
    }

    public static final Role system() {
        Role role = forName("system");
        if (null == role) {
            role = new Role();
            role.setType(Type.SYSTEM);
            role.setName("system");
            DeclarableDao declarableDao = Spring.bean("declarableDao");
            declarableDao.saveDeclarable(role);
        }
        return role;
    }
}
