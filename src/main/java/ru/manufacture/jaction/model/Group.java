package ru.manufacture.jaction.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import ru.manufacture.jaction.security.IGroup;
import ru.manufacture.persist.Declarable;

/**
 * @author Degtyarev Roman
 * @date 26.08.2015.
 */
@Entity
@Table(name = "J_GROUP")
public class Group extends Declarable implements IGroup {
    public static final String ANY = "ANY";
    public static final IGroup Any = new Group(){{setName(ANY);}};
}
