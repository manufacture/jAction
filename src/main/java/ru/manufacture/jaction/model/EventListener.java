package ru.manufacture.jaction.model;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import ru.manufacture.persist.Declarable;
import ru.manufacture.types.format.Format;

/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
@Entity
@Table(name = "J_EVENT_LISTENER")
public class EventListener extends Declarable {
    private Set<String> listen;
    private String listenAsString;

    @Column(name = "listen", length = 1024, nullable = false)
    public String getListenAsString() {
        return listenAsString;
    }

    public void setListenAsString(String listenAsString) {
        this.listenAsString = listenAsString;
        this.listen = Format.toObject(Set.class, listenAsString);
    }

    public void setListen(Set<String> listen) {
        this.listen = listen;
        this.listenAsString = Format.instance(Set.class).format(listen);
    }

    @Transient
    public Set<String> getListen() {
        return listen;
    }

    @Override
    public String toString() {
        return "listener[" + getName() + "] listen" + listen;
    }
}
