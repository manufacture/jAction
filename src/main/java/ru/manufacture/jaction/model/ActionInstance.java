/**
 * @author Degtyarev Roman
 * @date 20.08.2015.
 */
package ru.manufacture.jaction.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.springframework.util.ClassUtils;
import ru.manufacture.jaction.protocol.Command;
import ru.manufacture.jaction.support.algorithm.JAction;
import ru.manufacture.persist.Identity;

/**
 * экземпляр работы процесса
 */
@Entity
@Table(name = "J_ACTION_INSTANCE")
public class ActionInstance implements Serializable, Identity<Long> {
    /**
     * статус экземпляра работы
     */
    public enum Status {
        /**
         * работа активна
         */
        ACTIVE,
        /**
         * работа завершена
         */
        COMPLETE
    }

    /**
     * константа для обозначения отсутствия необходимого экземпляра работы
     */
    public static final ActionInstance NO_ACTION = new ActionInstance(){{setId(0);}};
    /**
     * идентификатор объекта
     */
    private long id;
    /**
     * экземпляр процесса
     */
    private ProcessInstance processInstance;
    /**
     * работа (шаг) процесса
     */
    private Action action;
    /**
     * дата/время создания работы
     */
    private Date createDate;
    /**
     * дата/время завершения работы
     */
    private Date completeDate;
    /**
     * имя (учетная запись) пользователя создавшего экземпляр работы
     */
    private String createUser;
    /**
     * имя (учетная запись) пользователя завершившего работу
     */
    private String completeUser;
    /**
     * группа, которой доступен экземпляр работы
     * определяется в момент создания экземпляра с помощью диспетчера работ {@link ru.manufacture.jaction.support.dispatch.ProcessDispatcher#dispatch(ActionInstance)}
     * @see ru.manufacture.jaction.support.dispatch.DefaultProcessDispatcher#dispatch(ActionInstance)
     */
    private Group group;
    /**
     * этап (контрольная точка) процесса
     */
    private Milestone milestone;
    /**
     * статус экземпляра работы
     */
    private Status status;
    /**
     * описание генерируется в момент создания экземпляра с помощью {@link ru.manufacture.jaction.support.algorithm.AlgorithmEngine#createDescription(ActionInstance)}
     * @see ru.manufacture.jaction.support.algorithm.DefaultAlgorithmEngine#createDescription(ActionInstance)
     */
    private String description;

    /**
     * @return идентификатор объекта
     */
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="jaction_seq")
    @SequenceGenerator(name="jaction_seq", sequenceName="jaction_seq", allocationSize = 1)
    public Long getId() {
        return id;
    }

    /**
     * @return экземпляр процесса
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "pi_id", nullable = false)
    public ProcessInstance getProcessInstance() {
        return processInstance;
    }

    /**
     * @return работа (шаг) процесса
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "action_id", nullable = false)
    public Action getAction() {
        return action;
    }

    /**
     * @return дата/время создания работы
     */
    @Column(name = "create_date", nullable = false)
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @return дата/время завершения работы
     */
    @Column(name = "complete_date")
    public Date getCompleteDate() {
        return completeDate;
    }

    /**
     * @return имя (учетная запись) пользователя создавшего экземпляр работы
     */
    @Column(name = "create_user", nullable = false)
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @return имя (учетная запись) пользователя завершившего работу
     */
    @Column(name = "complete_user")
    public String getCompleteUser() {
        return completeUser;
    }

    /**
     * @return группа, которой доступен экземпляр работы
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id", nullable = false)
    public Group getGroup() {
        return group;
    }

    /**
     * @return этап (контрольная точка) процесса
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "milestone_id")
    public Milestone getMilestone() {
        return milestone;
    }

    /**
     * @return статус экземпляра работы
     */
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    public Status getStatus() {
        return status;
    }

    /**
     * @return описание
     */
    @Column(name = "description", nullable = false)
    public String getDescription() {
        return description;
    }

    /**
     * @param id идентификатор объекта
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @param processInstance экземпляр процесса
     */
    public void setProcessInstance(ProcessInstance processInstance) {
        this.processInstance = processInstance;
    }

    /**
     * @param action работа (шаг) процесса
     */
    public void setAction(Action action) {
        this.action = action;
    }

    /**
     * @param createDate дата/время создания работы
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @param completeDate дата/время завершения работы
     */
    public void setCompleteDate(Date completeDate) {
        this.completeDate = completeDate;
    }

    /**
     * @param createUser имя (учетная запись) пользователя создавшего экземпляр работы
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @param completeUser имя (учетная запись) пользователя завершившего работу
     */
    public void setCompleteUser(String completeUser) {
        this.completeUser = completeUser;
    }

    /**
     * @param group группа, которой доступен экземпляр работы
     */
    public void setGroup(Group group) {
        this.group = group;
    }

    /**
     * @param milestone этап (контрольная точка) процесса
     */
    public void setMilestone(Milestone milestone) {
        this.milestone = milestone;
    }

    /**
     * @param status статус экземпляра работы
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @param description описание
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return true - если статус работы "работа активна"
     */
    @Transient
    public boolean isActive() {
        return Status.ACTIVE == status;
    }

    /**
     * @return true - если статус работы "работа завершена"
     */
    @Transient
    public boolean isComplete() {
        return Status.COMPLETE == status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ActionInstance that = (ActionInstance) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
