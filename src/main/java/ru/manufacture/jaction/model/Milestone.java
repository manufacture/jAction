package ru.manufacture.jaction.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import ru.manufacture.persist.Declarable;

/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
@Entity
@Table(name = "J_MILESTONE")
public class Milestone extends Declarable {

    @Override
    public String toString() {
        return "Milestone[" + getName() + "]";
    }
}
