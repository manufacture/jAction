/**
 * @author Degtyarev Roman
 * @date 28.08.2015.
 */
package ru.manufacture.jaction.model.view;

import java.util.Date;
import ru.manufacture.util.pageable.PageableMetadata;

/**
 * представление экземпляра работы пользователя
 */
public class VActionInstance implements PageableMetadata {
    /**
     * общее количество работ пользователя соответсвующее фильтру {@link ru.manufacture.jaction.model.view.ActionInstanceFilter}
     */
    private long totalRowsCount;
    /**
     * порядковый номер строки
     */
    private long rowNumber;
    /**
     * идентификатор экземпляра работы
     */
    private long actionInstance;
    /**
     * имя процесса
     */
    private String processName;
    /**
     * описание процесса
     */
    private String processDescription;
    /**
     * имя работы
     */
    private String actionName;
    /**
     * описание работы
     */
    private String actionDescription;
    /**
     * дата создания экземпляра работы
     */
    private Date createDate;
    /**
     * описание работы
     */
    private String description;
    /**
     * имя роли
     */
    private String roleName;
    /**
     * имя группы
     */
    private String groupName;
    /**
     * название этапа (контрольной точки)
     */
    private String milestone;

    public long getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Number rowNumber) {
        this.rowNumber = rowNumber.longValue();
    }

    public long getActionInstance() {
        return actionInstance;
    }

    public void setActionInstance(Number actionInstance) {
        this.actionInstance = actionInstance.longValue();
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getMilestone() {
        return milestone;
    }

    public void setMilestone(String milestone) {
        this.milestone = milestone;
    }

    public void setTotalRowsCount(Number totalRowsCount) {
        this.totalRowsCount = totalRowsCount.longValue();
    }

    @Override
    public long getTotalRowsCount() {
        return this.totalRowsCount;
    }

    public String getProcessDescription() {
        return processDescription;
    }

    public void setProcessDescription(String processDescription) {
        this.processDescription = processDescription;
    }

    public String getActionDescription() {
        return actionDescription;
    }

    public void setActionDescription(String actionDescription) {
        this.actionDescription = actionDescription;
    }
}
