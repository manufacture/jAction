/**
 * @author Degtyarev Roman
 * @date 28.08.2015.
 */
package ru.manufacture.jaction.model.view;

import java.io.Serializable;
import java.util.Date;
import ru.manufacture.types.Interval;
import ru.manufacture.util.pageable.SimplePageable;


import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * фильтр поиска работ пользователя
 */
public class ActionInstanceFilter implements Serializable {
    /**
     * интервал дат, за который показывать работы
     */
    private Interval<Date> dateInterval;
    /**
     * идентификатор процесса, по которому показывать работы
     */
    private Long processId;
    /**
     * описание работы
     */
    private String description;
    /**
     * количество записей на странице (для постраничного вывода)
     */
    private int pageSize;
    /**
     * текущая страница (для постраничного вывода)
     */
    private long currentPage;

    /**
     * конструктор
     * @param dateInterval интервал дат, за который показывать работы
     * @param processId идентификатор процесса, по которому показывать работы
     * @param description описание работы
     * @param pageSize количество записей на странице (для постраничного вывода)
     * @param currentPage текущая страница (для постраничного вывода)
     */
    public ActionInstanceFilter(Interval<Date> dateInterval, Long processId, String description, Integer pageSize, Long currentPage) {
        this.dateInterval = dateInterval;
        this.processId = processId;
        this.description = description;
        this.pageSize = (pageSize == null || pageSize <= 0) ? SimplePageable.DEFAULT_PAGE_SIZE : pageSize;
        this.currentPage = (currentPage == null || currentPage <= 0) ? 1 : currentPage;
    }

    /**
     * Конструктор. Для постраничного вывода используется размер страницы по умолчанию {@link ru.manufacture.util.pageable.SimplePageable#DEFAULT_PAGE_SIZE}.
     * Номер текущей страницы 1
     * @param dateInterval интервал дат, за который показывать работы
     * @param processId идентификатор процесса, по которому показывать работы
     * @param description описание работы
     */
    public ActionInstanceFilter(Interval<Date> dateInterval, Long processId, String description) {
        this(dateInterval, processId, description, 0, 0L);

    }

    /**
     * Конструктор. Для постраничного вывода используется размер страницы по умолчанию {@link ru.manufacture.util.pageable.SimplePageable#DEFAULT_PAGE_SIZE}.
     * Номер текущей страницы 1
     * @param dateInterval интервал дат, за который показывать работы
     */
    public ActionInstanceFilter(Interval<Date> dateInterval) {
        this(dateInterval, null, null, 0, 0L);

    }

    /**
     * @return интервал дат, за который показывать работы
     */
    public Interval<Date> getDateInterval() {
        return dateInterval;
    }

    /**
     * @return идентификатор процесса, по которому показывать работы
     */
    public Long getProcessId() {
        return processId;
    }

    /**
     * @return описание работы
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return маска описания работы
     */
    public String getDescriptionMask() {
        if (!isBlank(description) && !description.contains("%")) {
            return  "%" + this.description.trim() + "%";
        } else {
            return description;
        }
    }

    /**
     * @return количество записей на странице (для постраничного вывода)
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * @return текущая страница (для постраничного вывода)
     */
    public long getCurrentPage() {
        return currentPage;
    }

    public SimplePageable<VActionInstance> createPage() {
        return new SimplePageable<>(getCurrentPage(), getPageSize());
    }
}
