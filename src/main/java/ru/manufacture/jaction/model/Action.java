/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
package ru.manufacture.jaction.model;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import ru.manufacture.persist.Declarable;

/**
 * Работа (шаг) процесса
 */
@Entity
@Table(name = "J_ACTION",
        uniqueConstraints = @UniqueConstraint(name="J_ACTION_INDX1", columnNames = {"PROCESS_ID", "NAME"})
)
@AttributeOverride(name = "NAME", column = @Column(name = "NAME", nullable = false, unique = false))
public class Action extends Declarable {
    public static final String CALL_PROCESS = "CALL_PROCESS";
    /**
     * роль пользователя выполняющего работу
     */
    private Role role;
    /**
     * алгоритм работы
     */
    private Algorithm algorithm;
    /**
     * процесс, в который входит работа
     */
    private Process process;


    /**
     * @return возвращает алгоритм работы
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "algorithm_id", nullable = false)
    public Algorithm getAlgorithm() {
        return algorithm;
    }

    /**
     * @return возвращает роль пользователя
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id", nullable = false)
    public Role getRole() {
        return role;
    }

    /**
     * @return возвращает процесс, в который входит работа
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "process_id", nullable = false)
    public Process getProcess() {
        return process;
    }

    /**
     * @param process процесс, в который входит работа
     */
    public void setProcess(Process process) {
        this.process = process;
    }

    /**
     * @param algorithm алгоритм работы
     */
    public void setAlgorithm(Algorithm algorithm) {
        this.algorithm = algorithm;
    }

    /**
     * @param role роль пользователя
     */
    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "action[" + getName() + "] role[" + role + "] algorithm[" + algorithm + "]";
    }
}
