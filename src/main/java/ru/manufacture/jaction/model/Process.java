package ru.manufacture.jaction.model;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;
import ru.manufacture.jaction.lang.Body;
import ru.manufacture.jaction.lang.Container;
import ru.manufacture.jaction.lang.Statement;
import ru.manufacture.jaction.support.ProcessService;
import ru.manufacture.persist.Declarable;

/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
@Entity
@Table(name = "J_PROCESS")
public class Process extends Declarable {
    private String descriptorText;

    @Transient
    public ProcessDescriptor descriptor() {
        return ProcessDescriptor.get(getName(), descriptorText);
    }

    @Lob
    @Column(name = "descriptor", nullable = false)
    public String getDescriptor() {
        return descriptorText;
    }

    public void setDescriptor(String descriptor) {
        this.descriptorText = descriptor;
    }

    /*@OneToMany(fetch = FetchType.LAZY, mappedBy = "process")
    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }*/

    public ProcessInstance newInstance() {
        return newInstance(null);
    }

    public ProcessInstance newInstance(ActionInstance parentActionInstance) {
        ProcessInstance instance = new ProcessInstance();
        instance.setProcess(this);
        instance.setCreateDate(new Date());
        instance.setStatus(ProcessInstance.Status.ACTIVE);
        instance.setParentActionInstance(parentActionInstance);
        ProcessService.instance().saveProcessInstance(instance);
        return instance;
    }

    @Transient
    public Statement getNextStatement(Long statementId) {
        if (null == statementId) {
            return getBody().getNext(Container.FIRST_INDEX);
        } else {
            Statement statement = descriptor().getStatements().get(statementId);
            return statement.getParent().getNext(statement.getIndex() + 1);
        }
    }

    @Transient
    public Statement getStatement(long statementId) {
        return getStatements().get(statementId);
    }

    @Transient
    public Map<Long, ? extends Statement> getStatements() {
        return descriptor().getStatements();
    }

    @Transient
    public Body getBody() {
        return descriptor().getBody();
    }

    @Transient
    public Declaration getDeclaration() {
        return descriptor();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Process process = (Process) o;

        if (!getName().equals(process.getName())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public String toString() {
        return "Process{" +
                "name='" + getName() + '\'' +
                '}';
    }

    @Transient
    public List<EventListener> getListeners(Event event) {
        List<EventListener> listeners = new LinkedList<EventListener>();
        for (EventListener listener : getDeclaration().get(EventListener.class)) {
            if (listener.getListen().contains(event.getName())) {
                listeners.add(listener);
            }
        }
        return listeners;
    }
}
