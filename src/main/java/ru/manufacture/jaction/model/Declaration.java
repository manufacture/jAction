package ru.manufacture.jaction.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import ru.manufacture.persist.Declarable;
import ru.manufacture.persist.DeclarableDao;
import ru.manufacture.spring.Spring;

/**
 * @date 02.08.2016.
 */
public class Declaration extends Declarable {
    public static final Declaration Empty = new Declaration(Collections.EMPTY_LIST, Collections.EMPTY_MAP);

    private Map<String, Declaration> imports;
    private Map<Key, Declarable> data;

    public Declaration(Declaration declaration) {
        this.data = declaration.data;
        this.imports = declaration.imports;
    }

    public Declaration(Collection<? extends Declarable> data, Map<String, Declaration> imports) {
        this.data = new ConcurrentHashMap<>();
        for (Declarable declarable : data) {
            Key key = new Key(declarable.getClass(), declarable.getName());
            this.data.put(key, declarable);
        }
        this.imports = imports;
    }

    public Declaration(Map<String, Declaration> imports) {
        this(Collections.EMPTY_LIST, imports);
    }

    public void add(List<? extends Declarable> declarables) {
        for (Declarable declarable : declarables) {
            Key key = new Key(declarable.getClass(), declarable.getName());
            this.data.put(key, declarable);
        }
    }

    public <Type extends Declarable> Type get(Class<Type> type, String name) {
        return (Type) get(new Key(type, name));
    }

    private Declarable get(Key key) {
        Declarable declarable = data.get(key);
        if (declarable != null) {
            return declarable;
        }
        for (Declaration declaration : imports.values()) {
            declarable = declaration.get(key);
            if (declarable != null) {
                DeclarableDao declarableDao = Spring.bean("declarableDao");
                return declarableDao.getDeclarable(declarable.getClass(), declarable.getName());
            }
        }
        return null;
    }

    public <Type extends Declarable> Collection<Type> get(Class<Type> type) {
        Collection<Type> result = new ArrayList<>();
        for (Declarable declarable : data.values()) {
            if (type.isAssignableFrom(declarable.getClass())) {
                result.add((Type) declarable);
            }
        }
        return result;
    }

    private static class Key {
        Class<? extends Declarable> type;
        String name;

        public Key(Class<? extends Declarable> type, String name) {
            this.type = type;
            this.name = name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Key key = (Key) o;

            if (!name.equals(key.name)) return false;
            if (!type.equals(key.type)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = type.hashCode();
            result = 31 * result + name.hashCode();
            return result;
        }
    }

    public enum Type {
        Process("process"),
        Declare("declare"),
        Algorithms("algorithms"),
        Roles("roles"),
        Milestones("milestones"),
        Events("events"),
        EventListeners("event-listeners");

        private String element;

        Type(String element) {
            this.element = element;
        }

        public static Type of(String value) {
            for (Type type : values()) {
                if (type.element.equalsIgnoreCase(value)) {
                    return type;
                }
            }
            throw new IllegalArgumentException("Declaration not supported: " + value);
        }
    }
}
