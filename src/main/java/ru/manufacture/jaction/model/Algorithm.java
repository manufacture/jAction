/**
 * @author Degtyarev Roman
 * @date 21.08.2015.
 */
package ru.manufacture.jaction.model;

import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;
import ru.manufacture.java.JClassLoader;
import ru.manufacture.persist.Declarable;
import ru.manufacture.persist.DeclarableDao;
import ru.manufacture.spring.Spring;
import ru.manufacture.types.format.Format;

/**
 * Алгоритм. Определяет бизнес-логику и представление интерфейса пользователя
 */
@Entity
@Table(name = "J_ALGORITHM")
public class Algorithm extends Declarable {

    public static final String EMPTY = "empty";

    /**
     * тип исходного кода
     */
    public enum SourceType {
        /**
         * класс Java
         */
        CLASS,
        /**
         * боб spring
         */
        BEAN
    }

    /**
     * тип представления интерфейса пользователя
     */
    public enum ViewType {
        /**
         * Java Server Faces
         */
        JSF,
        /**
         * Java Server Pages
         */
        JSP,
        /**
         * Velocity шаблон
         */
        VELOCITY
    }

    /**
     * тип алгоритма
     */
    public enum Type {
        /**
         * системный алгоритм выполняется автоматически и не имеет представления графического интерфейса
         */
        SYSTEM_ACTION,
        /**
         * пользовательский алгоритм выполняется с участием человека и должен иметь представление графического интерфейса
         */
        HUMAN_ACTION
    }

    /**
     * тип алгоритма
     */
    private Type type;
    /**
     * тип исходного кода
     */
    private SourceType sourceType;
    /**
     * тип представления интерфейса пользователя
     */
    private ViewType viewType;
    /**
     * имя/расположение ресурса исходного кода бизнес логики
     */
    private String source;
    /**
     * имя/расположение ресурса исходного кода представления интерфейса пользователя
     */
    private String view;
    /**
     * глобальные параметры алгоритма
     */
    private Map<String, String> params;
    /**
     * глобальные параметры алгоритма в виде текста в формате *.properties
     */
    private String paramsAsString;

    /**
     * @return тип алгоритма
     */
    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    public Type getType() {
        return type;
    }

    /**
     * @return тип исходного кода
     */
    @Column(name = "source_type")
    @Enumerated(EnumType.STRING)
    public SourceType getSourceType() {
        return sourceType == null ? SourceType.CLASS : sourceType;
    }

    /**
     * @return тип представления интерфейса пользователя
     */
    @Column(name = "view_type")
    @Enumerated(EnumType.STRING)
    public ViewType getViewType() {
        return viewType;
    }

    /**
     * @return имя/расположение ресурса исходного кода бизнес логики
     */
    @Column(name = "src_name", length = 1024)
    public String getSource() {
        return source;
    }

    /**
     * @return имя/расположение ресурса исходного кода представления интерфейса пользователя
     */
    @Column(name = "view_name", length = 1024)
    public String getView() {
        return view;
    }

    /**
     * @return глобальные параметры алгоритма в виде текста в формате *.properties
     */
    @Lob
    @Column(name = "params")
    public String getParamsAsString() {
        return paramsAsString;
    }

    /**
     * @param paramsAsString глобальные параметры алгоритма в виде текста в формате *.properties
     */
    public void setParamsAsString(String paramsAsString) {
        this.paramsAsString = paramsAsString;
        this.params = Format.toObject(Map.class, paramsAsString);
    }

    /**
     * @return глобальные параметры алгоритма
     */
    @Transient
    public Map<String, String> getParams() {
        return params;
    }

    /**
     * @param type тип алгоритма
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * @param sourceType тип исходного кода
     */
    public void setSourceType(SourceType sourceType) {
        this.sourceType = null == sourceType ? SourceType.CLASS : sourceType;
    }

    /**
     * @param viewType тип представления интерфейса пользователя
     */
    public void setViewType(ViewType viewType) {
        this.viewType = viewType;
    }

    /**
     * @param source имя/расположение ресурса исходного кода бизнес логики
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @param view имя/расположение ресурса исходного кода представления интерфейса пользователя
     */
    public void setView(String view) {
        this.view = view;
    }

    /**
     * @param params глобальные параметры алгоритма
     */
    public void setParams(Map<String, String> params) {
        this.params = params;
        this.paramsAsString = Format.instance(Map.class).format(params);
    }

    /**
     * @return экземпляр класса алгоритма
     */
    public <Type> Type instance() {
        try {
            switch (getSourceType()) {
                case BEAN: return Spring.bean(getSource());
                case CLASS: {
                    Class<?> aClass = JClassLoader.classForName(getSource());
                    return (Type) aClass.newInstance();
                }
            }
            throw new InstantiationException("Source type not supported: " + getSourceType());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new IllegalStateException("Can't create algorithm[" + getName() + "] instance: " + getSource(), e);
        }
    }

    @Transient
    public boolean isEmpty() {
        return EMPTY.equalsIgnoreCase(getName());
    }

    public static boolean isEmpty(Algorithm algorithm) {
        return null == algorithm || algorithm.isEmpty();
    }

    public static Algorithm forName(String name) {
        DeclarableDao declarableDao = Spring.bean("declarableDao");
        return declarableDao.getDeclarable(Algorithm.class, name);
    }

    public static Algorithm empty() {
        Algorithm algorithm = forName(EMPTY);
        if (null == algorithm) {
            algorithm = new Algorithm();
            algorithm.setType(Type.SYSTEM_ACTION);
            algorithm.setName(EMPTY);
            DeclarableDao declarableDao = Spring.bean("declarableDao");
            declarableDao.saveDeclarable(algorithm);
        }
        return algorithm;
    }
}
