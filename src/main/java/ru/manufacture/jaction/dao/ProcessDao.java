/**
 * @author Degtyarev Roman
 * @date 26.08.2015.
 */
package ru.manufacture.jaction.dao;

import java.util.List;
import ru.manufacture.jaction.model.Action;
import ru.manufacture.jaction.model.ActionInstance;
import ru.manufacture.jaction.model.Process;
import ru.manufacture.jaction.model.ProcessInstance;
import ru.manufacture.jaction.model.Var;
import ru.manufacture.jaction.model.VarValue;
import ru.manufacture.jaction.model.view.ActionInstanceFilter;
import ru.manufacture.jaction.model.view.VActionInstance;
import ru.manufacture.jaction.security.IUserCredential;
import ru.manufacture.util.pageable.SimplePageable;

/**
 * ДАО процессов
 */
public interface ProcessDao {
    /**
     * создает описание процесса
     * @param process описание процесса
     * @throws IllegalStateException - если процесс с именем process.name уже существует
     * @return идентификатор записи
     */
    long createProcess(Process process);

    /**
     * обновляет описание процесс
     * @param process описание процесса
     * @return идентификатор записи
     */
    long updateProcess(Process process);

    /**
     * Сохраняет экземпляр процесса. Если заполнен id, то обновляет запись
     * @param processInstance экземпляр процесса
     */
    void saveProcessInstance(ProcessInstance processInstance);

    /**
     * возвращает экземпляр процесса
     * @param processInstanceId идетификатор процесса
     * @return экземпляр процесса
     */
    ProcessInstance getProcessInstance(long processInstanceId);

    /**
     * Сохраняет экземпляр работы процесса. Если заполнен id, то обновляет запись
     * @param actionInstance экземпляр работы процесса
     */
    void saveActionInstance(ActionInstance actionInstance);

    /**
     * возвращает экземпляр работы процесса
     * @param actionInstanceId идентификатор работы
     * @return экземпляр работы процесса
     */
    ActionInstance getActionInstance(long actionInstanceId);

    /**
     * сохраняет значение переменной в контекст экземпляра процесса
     * @param processInstance экземпляр процесса
     * @param var переменная контекста
     * @param value значение переменной
     */
    void saveVarValue(ProcessInstance processInstance, Var var, Object value);

    /**
     * возвращает значение переменной контекста экземпляра процесса
     * @param processInstance экземпляр процесса
     * @param var переменная контекста
     * @return значение переменной
     */
    VarValue getVarValue(ProcessInstance processInstance, Var var);

    /**
     * возвращает описание работы процесса
     * @param process описание процесса
     * @param actionName имя работы
     * @return описание работы
     */
    Action getAction(Process process, String actionName);

    /**
     * возвращает список работ пользователя
     * @param user пользователь
     * @param filter фильтр работ
     * @return страница работ пользователя
     */
    SimplePageable<VActionInstance> getActionInstances(IUserCredential user, ActionInstanceFilter filter);

    /**
     * Сохраняет описание работы процесса. Если заполнен id, то обновляет запись
     * @param action описание работы процесса
     */
    void saveAction(Action action);

    /**
     * @return возвращает список процессов
     */
    List<Process> getProcesses();
}
