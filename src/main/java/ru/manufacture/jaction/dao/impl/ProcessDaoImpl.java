/**
 * @author Degtyarev Roman
 * @date 26.08.2015.
 */
package ru.manufacture.jaction.dao.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.Validate;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import ru.manufacture.jaction.dao.ProcessDao;
import ru.manufacture.jaction.model.Action;
import ru.manufacture.jaction.model.ActionInstance;
import ru.manufacture.jaction.model.Group;
import ru.manufacture.jaction.model.Process;
import ru.manufacture.jaction.model.ProcessInstance;
import ru.manufacture.jaction.model.Role;
import ru.manufacture.jaction.model.Var;
import ru.manufacture.jaction.model.VarValue;
import ru.manufacture.jaction.model.view.ActionInstanceFilter;
import ru.manufacture.jaction.model.view.VActionInstance;
import ru.manufacture.jaction.security.IGroup;
import ru.manufacture.jaction.security.IRole;
import ru.manufacture.jaction.security.IUserCredential;
import ru.manufacture.persist.Declarable;
import ru.manufacture.persist.DeclarableDao;
import ru.manufacture.spring.Hibernate;
import ru.manufacture.util.DateUtil;
import ru.manufacture.util.pageable.SimplePageable;

/**
 * ДАО процессов (хибернейт реализация)
 */
public class ProcessDaoImpl extends Hibernate implements ProcessDao {
    @Autowired
    private DeclarableDao declarableDao;

    /**
     * @see ProcessDao#createProcess(ru.manufacture.jaction.model.Process)
     */
    @Override
    public long createProcess(ru.manufacture.jaction.model.Process process) {
        Process exists = declarableDao.getDeclarable(Process.class, process.getName());
        if (null != exists) {
            throw new IllegalStateException("Process already exists: " + process.getName());
        }
        session().saveOrUpdate(process);
        return process.getId();
    }

    /**
     * @see ProcessDao#updateProcess(ru.manufacture.jaction.model.Process)
     */
    @Override
    public long updateProcess(Process process) {
        session().saveOrUpdate(process);
        return process.getId();
    }

    /**
     * @see ProcessDao#saveProcessInstance(ru.manufacture.jaction.model.ProcessInstance)
     */
    @Override
    public void saveProcessInstance(ProcessInstance processInstance) {
        session().saveOrUpdate(processInstance);
    }

    /**
     * @see ProcessDao#getProcessInstance(long)
     */
    @Override
    public ProcessInstance getProcessInstance(long processInstanceId) {
        return (ProcessInstance) session().get(ProcessInstance.class, processInstanceId);
    }

    /**
     * @see ProcessDao#saveActionInstance(ru.manufacture.jaction.model.ActionInstance)
     */
    @Override
    public void saveActionInstance(ActionInstance actionInstance) {
        session().saveOrUpdate(actionInstance);
    }

    /**
     * @see ProcessDao#getActionInstance(long)
     */
    @Override
    public ActionInstance getActionInstance(long actionInstanceId) {
        return (ActionInstance) session().get(ActionInstance.class, actionInstanceId);
    }

    /**
     * @see ProcessDao#saveVarValue(ru.manufacture.jaction.model.ProcessInstance, ru.manufacture.jaction.model.Var, Object)
     */
    @Override
    public void saveVarValue(ProcessInstance processInstance, Var var, Object value) {
        VarValue varValue = getVarValue(processInstance, var);
        if (null == varValue) {
            varValue = new VarValue();
            varValue.setName(var.getName());
            varValue.setType(var.getType());
            varValue.setProcessInstance(processInstance);
            varValue.setProcessDate(processInstance.getCreateDate());
        }
        varValue.set(value);
        session().saveOrUpdate(varValue);
    }

    /**
     * @see ProcessDao#getVarValue(ru.manufacture.jaction.model.ProcessInstance, ru.manufacture.jaction.model.Var)
     */
    @Override
    public VarValue getVarValue(ProcessInstance processInstance, Var var) {
        String hql = "from VarValue as v where v.processInstance.id = :piid and v.name = :name and v.processDate = :date";
        Query query = session().createQuery(hql)
                .setParameter("piid", processInstance.getId())
                .setParameter("name", var.getName())
                .setParameter("date", processInstance.getCreateDate());
        List list = query.list();
        return list.isEmpty() ? null : (VarValue)list.get(0);
    }

    /**
     * @see ProcessDao#getAction(ru.manufacture.jaction.model.Process, String)
     */
    @Override
    public Action getAction(Process process, String actionName) {
        String hql = "from Action as a where a.process.id = :procId and a.name = :actName";
        Query query = session().createQuery(hql)
                .setParameter("procId", process.getId())
                .setParameter("actName", actionName);
        List list = query.list();
        return list.isEmpty() ? null : (Action) list.get(0);
    }

    /**
     * @see ProcessDao#getActionInstances(ru.manufacture.jaction.security.IUserCredential, ru.manufacture.jaction.model.view.ActionInstanceFilter)
     */
    @Override
    public SimplePageable<VActionInstance> getActionInstances(IUserCredential user, ActionInstanceFilter filter) {
        Validate.notNull(filter.getDateInterval().getFromValue(), "Date interval undefined");
        Collection<Long> groups = getGroups(user.getGroups());
        Collection<Long> roles = getRoles(user.getRoles());
        SimplePageable<VActionInstance> page = filter.createPage();
        List<VActionInstance> data;
        if (roles.isEmpty() || groups.isEmpty()) {
            data = Collections.emptyList();
        } else {
            String hql =
                    "select * from (\n" +
                            "  select ai.id \"actionInstance\"\n" +
                            "        ,p.description \"processDescription\"\n" +
                            "        ,a.description \"actionDescription\"\n" +
                            "        ,p.name \"processName\"\n" +
                            "        ,a.name \"actionName\"\n" +
                            "        ,ai.create_date \"createDate\"\n" +
                            "        ,ai.description \"description\"\n" +
                            "        ,r.description \"roleName\"\n" +
                            "        ,g.description \"groupName\"\n" +
                            "        ,m.description \"milestone\"\n" +
                            "        ,row_number() over(order by ai.id desc) \"rowNumber\"\n" +
                            "        ,count(*) over() \"totalRowsCount\"\n" +
                            "  from J_ACTION_INSTANCE ai\n" +
                            "      join J_ACTION a on a.id = ai.action_id\n" +
                            "      join J_PROCESS_INSTANCE pi on pi.id = ai.pi_id\n" +
                            "      join J_PROCESS p on p.id = pi.process_id\n" +
                            "      left join J_ROLE r on r.id = a.role_id\n" +
                            "      left join J_GROUP g on g.id = ai.group_id\n" +
                            "      left join J_MILESTONE m on m.id = pi.milestone_id\n" +
                            "  where ai.create_date >= :fromDate\n" +
                            "    and ai.create_date <= :toDate\n" +
                            "    and (:processId = 0 or pi.process_id = :processId)\n" +
                            "    and (:description is null or ai.description like :description)\n" +
                            "    and ai.status = :aiStatus\n" +
                            "    and pi.status = :piStatus\n" +
                            "    and ai.group_id in (:groups)\n" +
                            "    and a.role_id in (:roles)    \n" +
                            ") where \"rowNumber\" >= :fromRow\n" +
                            "    and \"rowNumber\" <= :toRow";
            Long processId = filter.getProcessId();
            Date toValue = filter.getDateInterval().getToValue();
            Query query = session().createSQLQuery(hql)
                    .setParameter("fromDate", filter.getDateInterval().getFromValue())
                    .setParameter("toDate", null == toValue ? DateUtil.POSITIVE_INFINITY_DATE : toValue)
                    .setParameter("processId", null == processId ? 0 : processId)
                    .setParameter("description", filter.getDescriptionMask())
                    .setParameter("aiStatus", ActionInstance.Status.ACTIVE.name())
                    .setParameter("piStatus", ProcessInstance.Status.ACTIVE.name())
                    .setParameterList("groups", groups.isEmpty() ? null : groups)
                    .setParameterList("roles", roles.isEmpty() ? null : roles)
                    .setParameter("fromRow", page.getFromRow())
                    .setParameter("toRow", page.getToRow())
                    .setResultTransformer(Transformers.aliasToBean(VActionInstance.class));
            data = query.list();
        }
        page.setData(data);
        return page;
    }

    /**
     * @see ProcessDao#saveAction(ru.manufacture.jaction.model.Action)
     */
    @Override
    public void saveAction(Action action) {
        declarableDao.saveDeclarable(action);
    }

    @Override
    public List<Process> getProcesses() {
        return session().createQuery("from Process as p order by p.name").list();
    }

    private Collection<Long> getRoles(Collection<IRole> roles) {
        Collection<String> names = Declarable.getNames(roles);
        List<? extends Declarable> list = declarableDao.getDeclarable(Role.class, names);
        return Declarable.getIds(list);
    }

    private Collection<Long> getGroups(Collection<IGroup> groups) {
        Collection<String> names = Declarable.getNames(groups);
        names.add(Group.ANY);
        List<? extends Declarable> list = declarableDao.getDeclarable(Group.class, names);
        return Declarable.getIds(list);
    }
}
