package ru.manufacture.jaction.support.algorithm;

/**
 * @author Degtyarev Roman
 * @date 19.10.2015.
 */
public interface CreateDescription {
    String createDescription();
}
