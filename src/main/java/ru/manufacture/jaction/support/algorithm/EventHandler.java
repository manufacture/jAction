package ru.manufacture.jaction.support.algorithm;

import ru.manufacture.jaction.lang.FireEvent;
import ru.manufacture.jaction.model.ProcessInstance;

/**
 * @date 27.07.2016.
 */
public interface EventHandler {
    void handle(ProcessInstance processInstance, FireEvent event);
}
