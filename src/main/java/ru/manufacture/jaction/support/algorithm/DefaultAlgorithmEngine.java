package ru.manufacture.jaction.support.algorithm;

import bsh.EvalError;
import java.util.Collection;
import java.util.Map;
import javax.persistence.Transient;
import org.springframework.util.ClassUtils;
import ru.manufacture.jaction.lang.FireEvent;
import ru.manufacture.jaction.model.ActionInstance;
import ru.manufacture.jaction.model.Algorithm;
import ru.manufacture.jaction.model.EventListener;
import ru.manufacture.jaction.model.ProcessInstance;
import ru.manufacture.jaction.model.Var;
import ru.manufacture.jaction.protocol.Command;
import ru.manufacture.jaction.protocol.ProcessRequest;
import ru.manufacture.spring.Spring;


import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 20.08.2015.
 */
public class DefaultAlgorithmEngine extends AlgorithmEngine {
    @Override
    public <Data> void executeSystemAction(ActionInstance actionInstance, ProcessRequest<Data> request) {
        ExecuteSystemAction action = getAlgorithmInstance(actionInstance, ExecuteSystemAction.class);
        if (null != action) {
            action.executeSystemAction(request);
        }
    }

    @Override
    public <Data> Data executeAction(ActionInstance actionInstance, ProcessRequest<Data> request) {
        ExecuteAction action = getAlgorithmInstance(actionInstance, ExecuteAction.class);
        if (null != action) {
            return (Data) action.executeAction(request);
        }
        return request.getData();
    }

    @Override
    public <Data> void completeAction(ActionInstance actionInstance, ProcessRequest<Data> request) {
        CompleteAction action = getAlgorithmInstance(actionInstance, CompleteAction.class);
        if (null != action) {
            action.completeAction(request);
        }
    }

    @Override
    public String createDescription(ActionInstance actionInstance) {
        CreateDescription action = getAlgorithmInstance(actionInstance, CreateDescription.class);
        String descr = null == action ? null : action.createDescription();
        return isBlank(descr) ? Algorithm.EMPTY : descr;
    }

    @Override
    public boolean executeBooleanStatement(String statement, ProcessInstance processInstance) {
        try {
            bsh.Interpreter interpreter = new bsh.Interpreter();
            interpreter.set("spring", Spring.context());
            Collection<Var> vars = processInstance.getProcess().getDeclaration().get(Var.class);
            for (Var var : vars) {
                Object value = processInstance.getVar(var.getName());
                Object defaultValue = var.getType().cast(var.getDefaultValue());
                interpreter.set(var.getName(), null != value ? value : defaultValue);
            }
            Object result = interpreter.eval(statement);
            return (Boolean) result;
        } catch (EvalError evalError) {
            throw new ScriptEvalException("Script evaluation fails:\n" + statement, evalError);
        }
    }

    @Override
    public void handleEvent(EventListener listener, FireEvent event, ProcessInstance processInstance) {
        Algorithm algorithm = Algorithm.forName(listener.getName());
        if (Algorithm.isEmpty(algorithm)) {
            throw new IllegalStateException("Algorithm for event listener not found: " + listener);
        }
        EventHandler handler = algorithm.instance();
        handler.handle(processInstance, event);
    }

    /**
     * @return экземпляр алгоритма
     */
    @Transient
    public <AlgorithmType> AlgorithmType getAlgorithmInstance(ActionInstance actionInstance, Class<? extends AlgorithmType> type) {
        Algorithm algorithm = actionInstance.getAction().getAlgorithm();
        if (Algorithm.isEmpty(algorithm)) {
            return null;
        }
        Object algorithmInstance = algorithm.instance();
        if (algorithmInstance instanceof JAction) {
            JAction jAction = (JAction) algorithmInstance;
            jAction.setActionInstance(actionInstance);
            jAction.setUserCredentials(Command.currentCommand().getUserCredential());
        }
        if (ClassUtils.isAssignable(type, algorithmInstance.getClass())) {
            return (AlgorithmType) algorithmInstance;
        }
        return null;
    }
}
