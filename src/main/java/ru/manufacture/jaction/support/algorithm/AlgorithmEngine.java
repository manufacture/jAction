package ru.manufacture.jaction.support.algorithm;

import ru.manufacture.jaction.lang.FireEvent;
import ru.manufacture.jaction.model.ActionInstance;
import ru.manufacture.jaction.model.EventListener;
import ru.manufacture.jaction.model.ProcessInstance;
import ru.manufacture.jaction.protocol.ProcessRequest;


import static ru.manufacture.util.ObjectUtil.getInstanceBySystemProperty;

/**
 * @author Degtyarev Roman
 * @date 20.08.2015.
 */
public abstract class AlgorithmEngine {
    private static class Single {
        static AlgorithmEngine tone = initialize();

        private static AlgorithmEngine initialize() {
            return getInstanceBySystemProperty(AlgorithmEngine.class, DefaultAlgorithmEngine.class);
        }
    }

    public static AlgorithmEngine instance() {
        return Single.tone;
    }

    public abstract <Data> void executeSystemAction(ActionInstance actionInstance, ProcessRequest<Data> request);

    public abstract <Data> Data executeAction(ActionInstance actionInstance, ProcessRequest<Data> request);

    public abstract <Data> void completeAction(ActionInstance actionInstance, ProcessRequest<Data> request);

    public abstract String createDescription(ActionInstance actionInstance);

    public abstract boolean executeBooleanStatement(String statement, ProcessInstance processInstance);

    public abstract void handleEvent(EventListener listener, FireEvent event, ProcessInstance processInstance);
}
