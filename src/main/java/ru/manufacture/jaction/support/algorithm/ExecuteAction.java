package ru.manufacture.jaction.support.algorithm;

import ru.manufacture.jaction.protocol.ProcessRequest;

/**
 * @author Degtyarev Roman
 * @date 19.10.2015.
 */
public interface ExecuteAction<Data> {
    Data executeAction(ProcessRequest<Data> request);
}
