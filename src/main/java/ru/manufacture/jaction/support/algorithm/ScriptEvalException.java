package ru.manufacture.jaction.support.algorithm;

/**
 * @author Degtyarev Roman
 * @date 20.10.2015.
 */
public class ScriptEvalException extends RuntimeException {
    public ScriptEvalException(String message) {
        super(message);
    }

    public ScriptEvalException(String message, Throwable cause) {
        super(message, cause);
    }
}
