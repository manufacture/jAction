package ru.manufacture.jaction.support.algorithm;

import java.util.Collections;
import java.util.Map;
import ru.manufacture.jaction.model.ActionInstance;
import ru.manufacture.jaction.model.Algorithm;
import ru.manufacture.jaction.model.Milestone;
import ru.manufacture.jaction.security.IGroup;
import ru.manufacture.jaction.security.IRole;
import ru.manufacture.jaction.security.IUserCredential;
import ru.manufacture.types.format.Format;

/**
 * @author Degtyarev Roman
 * @date 19.10.2015.
 */
public class JAction {
    private ActionInstance actionInstance;
    private IUserCredential userCredentials;

    public <Type> Type getVar(String name) {
        return getActionInstance().getProcessInstance().getVar(name);
    }

    public void setVar(String name, Object value) {
        getActionInstance().getProcessInstance().setVar(name, value);
    }

    public ActionInstance getActionInstance() {
        return actionInstance;
    }

    public void setActionInstance(ActionInstance actionInstance) {
        this.actionInstance = actionInstance;
    }

    public void setUserCredentials(IUserCredential userCredentials) {
        this.userCredentials = userCredentials;
    }

    public IUserCredential getUserCredentials() {
        return userCredentials;
    }

    public IGroup getGroup() {
        return actionInstance.getGroup();
    }

    public IRole getRole() {
        return actionInstance.getAction().getRole();
    }

    public Map<String, String> getParameters() {
        Algorithm algorithm = actionInstance.getAction().getAlgorithm();
        return null != algorithm ? algorithm.getParams() : Collections.EMPTY_MAP;
    }

    public <Type> Type getParameter(String name, Class<? extends Type> type) {
        String param = getParameters().get(name);
        return Format.toObject(type, param);
    }

    public Milestone getMilestone() {
        return actionInstance.getProcessInstance().getMilestone();
    }
}
