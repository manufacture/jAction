package ru.manufacture.jaction.support;

import java.util.Date;
import java.util.NoSuchElementException;
import ru.manufacture.jaction.lang.Statement;
import ru.manufacture.jaction.model.Milestone;
import ru.manufacture.jaction.model.ProcessInstance;
import ru.manufacture.jaction.protocol.Command;
import ru.manufacture.jaction.support.hook.ProcessHook;

/**
 * @author Degtyarev Roman
 * @date 10.12.2015.
 */
public class ProcessIterator {
    private ProcessInstance processInstance;


    public ProcessIterator(ProcessInstance processInstance) {
        this.processInstance = processInstance;
    }

    public Statement getNext() {
        Long statementId = processInstance.getStatementId();
        ru.manufacture.jaction.model.Process process = processInstance.getProcess();
        Statement current = null == statementId ? null : process.getStatement(statementId);
        Statement next = process.getNextStatement(statementId);
        return null == next ? null : move(current, next);
    }

    private Statement move(Statement current, Statement next) {
        if (null == next) {
            throw new NoSuchElementException("Process has no more statements");
        }
        ProcessHook hook = Command.currentCommand().getProcessHook();
        if (null == current) {
            processInstance.setStatus(ProcessInstance.Status.ACTIVE);
            hook.onStart(processInstance);
        } else {
            hook.onExit(processInstance, current);
        }
        processInstance.setStatementId(next.getId());
        if (next.getType() == Statement.Type.End) {
            processInstance.setStatus(ProcessInstance.Status.COMPLETE);
            processInstance.setCompleteDate(new Date());
            Milestone finalMilestone = processInstance.getProcess().getBody().getFinalMilestone();
            if (finalMilestone != null) {
                processInstance.setMilestone(finalMilestone);
            }
        }
        hook.onEnter(processInstance, next);
        ProcessService.instance().saveProcessInstance(processInstance);
        return next;
    }
}
