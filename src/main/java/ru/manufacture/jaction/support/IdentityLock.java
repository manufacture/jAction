package ru.manufacture.jaction.support;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import ru.manufacture.persist.Identity;

/**
 * @author Degtyarev Roman
 * @date 18.12.2015.
 */
public class IdentityLock<Type> {
    private final Object lock = new Object();
    private final Map<Type, Value<Type>> map = new TreeMap<>();

    public Value<Type> lock(Identity<Type> identity) {
        Value<Type> lockValue;
        Type id = identity.getId();
        synchronized (lock) {
            if (!map.containsKey(id)) {
                Value<Type> value = new Value<>();
                value.id = id;
                value.count = 0;
                value.lock = new ReentrantLock();
                map.put(id, value);
            }
            lockValue = map.get(id);
            lockValue.count++;
        }
        lockValue.lock.lock();
        return lockValue;
    }

    public void release(Value<Type> lockValue) {
        lockValue.lock.unlock();
        synchronized (lock) {
            lockValue.count--;
            if (lockValue.count == 0)
                map.remove(lockValue.id);
        }
    }

    public static class Value<T> {
        private Lock lock;
        private long count;
        private T id;
    }
}
