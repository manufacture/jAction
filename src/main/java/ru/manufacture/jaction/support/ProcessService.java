package ru.manufacture.jaction.support;

import java.util.List;
import ru.manufacture.jaction.model.Action;
import ru.manufacture.jaction.model.ActionInstance;
import ru.manufacture.jaction.model.Declaration;
import ru.manufacture.jaction.model.Group;
import ru.manufacture.jaction.model.Milestone;
import ru.manufacture.jaction.model.Process;
import ru.manufacture.jaction.model.ProcessDescriptor;
import ru.manufacture.jaction.model.ProcessInstance;
import ru.manufacture.jaction.model.Var;
import ru.manufacture.jaction.model.view.ActionInstanceFilter;
import ru.manufacture.jaction.model.view.VActionInstance;
import ru.manufacture.jaction.security.IUserCredential;
import ru.manufacture.spring.Spring;
import ru.manufacture.util.pageable.SimplePageable;

/**
 * @author Degtyarev Roman
 * @date 22.07.2015.
 */
public abstract class ProcessService {
    private static class Single {
        static ProcessService tone = initialize();

        private static ProcessService initialize() {
            return Spring.bean("processService");
        }
    }

    public static ProcessService instance() {
        return Single.tone;
    }

    public abstract Process deployProcess(ProcessDescriptor descriptor);

    public abstract void deploy(Declaration descriptor);

    public abstract Process getProcess(String processName);

    public abstract List<Process> getProcesses();

    public abstract void saveProcessInstance(ProcessInstance processInstance);

    public abstract void saveActionInstance(ActionInstance actionInstance);

    public abstract ProcessInstance loadProcessInstance(long pid);

    public abstract ActionInstance loadActionInstance(long actionInstanceId);

    public abstract <Type> void saveVariable(ProcessInstance processInstance, Var var, Type value);

    public abstract <Type> Type loadVariable(ProcessInstance processInstance, Var var);

    public abstract Group getOrCreateGroup(String groupName);

    public abstract Milestone getMilestone(String milestoneName);

    public abstract Action getAction(Process process, String actionName);

    public abstract SimplePageable<VActionInstance> getActionInstances(IUserCredential userCredential, ActionInstanceFilter filter);
}
