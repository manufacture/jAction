package ru.manufacture.jaction.support.dispatch;

import ru.manufacture.jaction.model.ActionInstance;
import ru.manufacture.jaction.model.Group;
import ru.manufacture.jaction.security.IGroup;

/**
 * @author Degtyarev Roman
 * @date 20.08.2015.
 */
public class DefaultProcessDispatcher extends ProcessDispatcher {
    @Override
    public IGroup dispatch(ActionInstance actionInstance) {
        return Group.Any;
    }
}
