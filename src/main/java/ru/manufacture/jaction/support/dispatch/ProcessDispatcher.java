package ru.manufacture.jaction.support.dispatch;

import ru.manufacture.jaction.model.ActionInstance;
import ru.manufacture.jaction.security.IGroup;


import static ru.manufacture.util.ObjectUtil.getInstanceBySystemProperty;

/**
 * @author Degtyarev Roman
 * @date 20.08.2015.
 */
public abstract class ProcessDispatcher {
    private static class Single {
        static ProcessDispatcher tone = initialize();

        private static ProcessDispatcher initialize() {
            return getInstanceBySystemProperty(ProcessDispatcher.class, DefaultProcessDispatcher.class);
        }
    }

    public static ProcessDispatcher instance() {
        return Single.tone;
    }

    public abstract IGroup dispatch(ActionInstance actionInstance);
}
