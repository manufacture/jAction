package ru.manufacture.jaction.support;

/**
 * @author Degtyarev Roman
 * @date 27.07.2015.
 */
public class InvalidProcessDeclarationException extends IllegalStateException {
    public InvalidProcessDeclarationException(String s) {
        super(s);
    }

    public InvalidProcessDeclarationException(String message, Throwable cause) {
        super(message, cause);
    }
}
