package ru.manufacture.jaction.support;

import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import ru.manufacture.jaction.dao.ProcessDao;
import ru.manufacture.jaction.model.Action;
import ru.manufacture.jaction.model.ActionInstance;
import ru.manufacture.jaction.model.Algorithm;
import ru.manufacture.jaction.model.Declaration;
import ru.manufacture.jaction.model.Event;
import ru.manufacture.jaction.model.EventListener;
import ru.manufacture.jaction.model.Group;
import ru.manufacture.jaction.model.Milestone;
import ru.manufacture.jaction.model.Process;
import ru.manufacture.jaction.model.ProcessDescriptor;
import ru.manufacture.jaction.model.ProcessInstance;
import ru.manufacture.jaction.model.Role;
import ru.manufacture.jaction.model.Var;
import ru.manufacture.jaction.model.VarValue;
import ru.manufacture.jaction.model.view.ActionInstanceFilter;
import ru.manufacture.jaction.model.view.VActionInstance;
import ru.manufacture.jaction.security.IUserCredential;
import ru.manufacture.persist.Declarable;
import ru.manufacture.persist.DeclarableDao;
import ru.manufacture.util.pageable.SimplePageable;

/**
 * @author Degtyarev Roman
 * @date 27.07.2015.
 */
public class DefaultProcessService extends ProcessService {
    @Autowired
    private ProcessDao processDao;
    @Autowired
    private DeclarableDao declarableDao;

    @Override
    public Process deployProcess(ProcessDescriptor descriptor) {
        Process process = declarableDao.getDeclarable(Process.class, descriptor.getName());
        if (null == process) {
            process = new Process();
            process.setName(descriptor.getName());
            process.setDescription(descriptor.getDescription());
            process.setDescriptor(descriptor.getSource());
            processDao.createProcess(process);
        } else {
            process.setDescription(descriptor.getDescription());
            process.setDescriptor(descriptor.getSource());
            processDao.updateProcess(process);
        }
        deploy(descriptor.get(Role.class));
        deploy(descriptor.get(Algorithm.class));
        deployActions(process, descriptor.get(Action.class));
        deploy(descriptor.get(Milestone.class));
        deploy(descriptor.get(Event.class));
        deploy(descriptor.get(EventListener.class));
        ProcessDescriptor.resetCache();
        return process;
    }

    @Override
    public void deploy(Declaration descriptor) {
        deploy(descriptor.get(Role.class));
        deploy(descriptor.get(Algorithm.class));
        deploy(descriptor.get(Milestone.class));
        deploy(descriptor.get(Event.class));
        deploy(descriptor.get(EventListener.class));
    }

    private void deployActions(Process process, Collection<Action> actions) {
        for (Action action : actions) {
            Action exists = processDao.getAction(process, action.getName());
            action.setProcess(process);
            if (null != exists) {
                action.setId(exists.getId());
                declarableDao.mergeDeclarable(action);
            } else {
                declarableDao.saveDeclarable(action);
            }
        }
    }

    private <Type extends Declarable> void deploy(Collection<Type> items) {
        for (Type item : items) {
            Declarable exists = declarableDao.getDeclarable(item.getClass(), item.getName());
            if (null != exists) {
                item.setId(exists.getId());
                declarableDao.mergeDeclarable(item);
            } else {
                declarableDao.saveDeclarable(item);
            }
        }
    }

    @Override
    public Process getProcess(String processName) {
        return declarableDao.getDeclarable(Process.class, processName);
    }

    @Override
    public List<Process> getProcesses() {
        return processDao.getProcesses();
    }

    @Override
    public void saveProcessInstance(ProcessInstance processInstance) {
        processDao.saveProcessInstance(processInstance);
    }

    @Override
    public ProcessInstance loadProcessInstance(long pid) {
        return processDao.getProcessInstance(pid);
    }

    @Override
    public void saveActionInstance(ActionInstance actionInstance) {
        processDao.saveActionInstance(actionInstance);
    }

    @Override
    public ActionInstance loadActionInstance(long actionInstanceId) {
        return processDao.getActionInstance(actionInstanceId);
    }

    @Override
    public <Type> void saveVariable(ProcessInstance processInstance, Var var, Type value) {
        processDao.saveVarValue(processInstance, var, value);
    }

    @Override
    public <Type> Type loadVariable(ProcessInstance processInstance, Var var) {
        VarValue value = processDao.getVarValue(processInstance, var);
        return null == value ? null : (Type) value.get();
    }

    @Override
    public Group getOrCreateGroup(String groupName) {
        Group group = declarableDao.getDeclarable(Group.class, groupName);
        if (null == group) {
            group = new Group();
            group.setName(groupName);
            group.setDescription(groupName);
            declarableDao.saveDeclarable(group);
        }
        return group;
    }

    @Override
    public Milestone getMilestone(String milestoneName) {
        return declarableDao.getDeclarable(Milestone.class, milestoneName);
    }

    @Override
    public Action getAction(Process process, String actionName) {
        return processDao.getAction(process, actionName);
    }

    @Override
    public SimplePageable<VActionInstance> getActionInstances(IUserCredential userCredential, ActionInstanceFilter filter) {
        return processDao.getActionInstances(userCredential, filter);
    }
}
