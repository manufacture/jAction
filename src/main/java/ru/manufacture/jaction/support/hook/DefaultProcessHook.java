package ru.manufacture.jaction.support.hook;

import java.util.List;
import ru.manufacture.jaction.lang.Call;
import ru.manufacture.jaction.lang.Execute;
import ru.manufacture.jaction.lang.FireEvent;
import ru.manufacture.jaction.lang.IncValue;
import ru.manufacture.jaction.lang.SetValue;
import ru.manufacture.jaction.model.EventListener;
import ru.manufacture.jaction.model.ProcessInstance;
import ru.manufacture.jaction.protocol.Command;
import ru.manufacture.jaction.support.algorithm.AlgorithmEngine;

/**
 * @author Degtyarev Roman
 * @date 21.07.2015.
 */
public class DefaultProcessHook extends ProcessHook {
    @Override
    public void onBeforeCallProcess(ProcessInstance processInstance, Call call) {
        System.out.println("Call subprocess " + call.getProcess());
    }

    @Override
    public void onAfterCallProcess(ProcessInstance processInstance, Call call) {
        System.out.println("Subprocess completed " + call.getProcess());
    }

    @Override
    public void onStart(ProcessInstance instance) {
        System.out.println("start " + instance.getProcess());
    }

    @Override
    public void onEnd(ProcessInstance instance) {
        System.out.println("end " + instance.getProcess());
    }

    @Override
    public void onBeforeExecute(ProcessInstance instance, Execute execute) {
        System.out.println("before execute " + execute);
    }

    @Override
    public void onAfterExecute(ProcessInstance instance, Execute execute) {
        System.out.println("after execute " + execute);
    }

    @Override
    public void onIncVariable(ProcessInstance instance, IncValue incValue) {
        System.out.println(incValue);
    }

    @Override
    public void onSetVariable(ProcessInstance instance, SetValue setValue) {
        System.out.println(setValue);
    }

    @Override
    public void onFireEvent(final ProcessInstance instance, final FireEvent event, List<EventListener> listeners) {
        final AlgorithmEngine engine = Command.currentCommand().getAlgorithmEngine();
        for (final EventListener listener : listeners) {
            new Thread(
                    new Runnable() {
                        @Override
                        public void run() {
                            engine.handleEvent(listener, event, instance);
                        }
                    }
            ).start();
        }
    }
}
