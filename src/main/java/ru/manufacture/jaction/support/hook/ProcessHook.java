package ru.manufacture.jaction.support.hook;

import java.util.List;
import java.util.Map;
import ru.manufacture.jaction.lang.Call;
import ru.manufacture.jaction.lang.Execute;
import ru.manufacture.jaction.lang.FireEvent;
import ru.manufacture.jaction.lang.IncValue;
import ru.manufacture.jaction.lang.SetValue;
import ru.manufacture.jaction.lang.Statement;
import ru.manufacture.jaction.model.EventListener;
import ru.manufacture.jaction.model.ProcessInstance;
import ru.manufacture.jaction.model.Var;
import ru.manufacture.jaction.support.ProcessService;


import static ru.manufacture.util.ObjectUtil.getInstanceBySystemProperty;

/**
 * @author Degtyarev Roman
 * @date 21.07.2015.
 */
public abstract class ProcessHook {
    private static class Single {
        static ProcessHook tone = initialize();

        private static ProcessHook initialize() {
            return getInstanceBySystemProperty(ProcessHook.class, DefaultProcessHook.class);
        }
    }

    public static ProcessHook instance() {
        return Single.tone;
    }

    public void onExit(ProcessInstance processInstance, Statement statement) {
        switch (statement.getType()) {
            case Execute: {
                onAfterExecute(processInstance, (Execute) statement);
                break;
            }
            case Call: {
                onAfterCallProcess(processInstance, (Call) statement);
            }
        }
    }

    public void onEnter(ProcessInstance processInstance, Statement statement) {
        switch (statement.getType()) {
            case Fire: {
                FireEvent fire = (FireEvent) statement;
                List<EventListener> listeners = processInstance.getProcess().getListeners(fire.getEvent());
                onFireEvent(processInstance, fire, listeners);
                break;
            }
            case Set: {
                SetValue setValue = (SetValue) statement;
                onSetVariable(processInstance, setValue);
                ProcessService.instance().saveVariable(processInstance, setValue.getVar(), setValue.getValue());
                break;
            }
            case Inc: {
                IncValue incValue = (IncValue) statement;
                onIncVariable(processInstance, incValue);
                Object resultValue = incValue.getResultValue(processInstance);
                ProcessService.instance().saveVariable(processInstance, incValue.getVar(), resultValue);
                break;
            }
            case Execute: {
                onBeforeExecute(processInstance, (Execute) statement);
                break;
            }
            case End: {
                onEnd(processInstance);
                break;
            }
            case Call: {
                onBeforeCallProcess(processInstance, (Call) statement);
                break;
            }
        }
    }

    public void setContext(ProcessInstance processInstance, Map<Var, Object> context) {
        if (null != context) {
            for (Map.Entry<Var, Object> entry : context.entrySet()) {
                SetValue setValue = new SetValue(0, entry.getKey(), entry.getValue());
                onEnter(processInstance, setValue);
            }
        }
    }

    public abstract void onBeforeCallProcess(ProcessInstance processInstance, Call call);

    public abstract void onAfterCallProcess(ProcessInstance processInstance, Call call);

    public abstract void onStart(ProcessInstance instance);

    public abstract void onEnd(ProcessInstance instance);

    public abstract void onBeforeExecute(ProcessInstance instance, Execute execute);

    public abstract void onAfterExecute(ProcessInstance instance, Execute execute);

    public abstract void onIncVariable(ProcessInstance instance, IncValue incValue);

    public abstract void onSetVariable(ProcessInstance instance, SetValue setValue);

    public abstract void onFireEvent(ProcessInstance instance, FireEvent fireEvent, List<EventListener> listeners);
}
