package ru.manufacture.jaction.support;

/**
 * @author Degtyarev Roman
 * @date 27.07.2015.
 */
public class ProcessNotFoundException extends IllegalArgumentException {
    public ProcessNotFoundException(String s) {
        super(s);
    }

    public ProcessNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
