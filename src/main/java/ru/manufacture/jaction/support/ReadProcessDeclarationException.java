package ru.manufacture.jaction.support;

/**
 * @date 29.07.2016.
 */
public class ReadProcessDeclarationException extends IllegalStateException {
    public ReadProcessDeclarationException(String message, Throwable cause) {
        super(message, cause);
    }
}
